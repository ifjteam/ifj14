/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */	
/*		Velecký Lukáš	xvelec05      								 */	
/*		Zemek Martin	xzemek04									 */	
/*																	 */
/*********************************************************************/

#ifndef SCANNER_H
#define	SCANNER_H

typedef struct {
    int type;
    char* string;
}token_t;

extern int row;
extern int character;

int getToken(token_t *token);
void openFile(FILE *f);
void freeToken(token_t *token);
int stringCompare(char* s1, char *s2);


// type= klicove slovo (integer, real)

#endif	/* SCANNER_H */

