/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */	
/*		Velecký Lukáš	xvelec05      								 */	
/*		Zemek Martin	xzemek04									 */	
/*																	 */
/*********************************************************************/

#ifndef ERROR_H
#define	ERROR_H


//makra
#define OK 0
#define ERROR_LEXICAL 1
#define ERROR_SYNTAX 2

// – nedefinovana funkce/promenna, pokus o redefinici funkce/promenne, atd.
#define ERROR_SEMANTIC 3
/* semanticka chyba typove kompatibility v aritmetických, retezcových a relacnich
 * vyrazech, prip. spatny pocet ci typ parametru u volani funkce.
 */
#define ERROR_SEMANTIC_TYPE 4
#define ERROR_SEMANTIC_OTHER 5
//behova chyba pri na citani ciselne hodnoty ze vstupu.
#define ERROR_RUN_INPUT 6
//behova chyba pri praci s neinicializovanou promennou.
#define ERROR_RUN_UNINITIALIZED 7
#define ERROR_RUN_DIV_BY_ZERO 8
#define ERROR_RUN_OTHER 9
/*
 *  * ERROR_INTERNAL - interní chyba interpretu tj. neovlivnena vstupnim programem 
 * (napr. chyba alokace pameti, chyba pri otvirani souboru s ridicim programem,
 *  spatne parametryprikazove rádky atd.).
 */
#define ERROR_INTERNAL 99

//prevede text v kodu (napriklad ENUM) na retezec
#define stringify( name ) # name

void warning(const char *fmt, ...);
/*
 * @brief Vytiskne chybove hlaseni "Chyba: " na stderr
 * Muze mit libovolny pocet argumentu
 * @example Warning(" lexikalni analyzy na radku: %d, radek);
 * @param *fmt retezec, ktery se vytiskne
 */

#endif	/* ERROR_H */

