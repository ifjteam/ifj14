/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */
/*		Velecký Lukáš	xvelec05      								 */
/*		Zemek Martin	xzemek04									 */
/*																	 */
/*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "error.h"
#include "parser.h"
#include "scanner.h"
#include "ial.h"
//#include "scannersim.h"
#include "psa.h"
#include "generator.h"
#include "main.h"
#include "interpret.h"

/*
 * 
 */
//globalni tabulka symbolu
htab_t* symbolTable;
//globalni seznam pro uvolneni naparsovanych listitem_t pro interpret 
listOfItemsFree_t* notFreeItems;

//globalni tabulka pro uvolneni lokalnich tabulek symbolu
tablesToFree_t* notFreeTables;
//globalni list instrukci, programu i funkci
tListOfInstr instructionList;

int main(int argc, char** argv) {
    int result = OK;
    notFreeItems = NULL;  //inicializace

    FILE *code; //TEST PRO SCANNER
    if (argc > 1) {
        if ((code = fopen(argv[1], "r")) == NULL) {
            warning("Soubor se nepodarilo otevrit %s", argv[1]);
            return ERROR_INTERNAL;
        }
    } else {
        warning("Nebyl zadan vstupni soubor");
        return ERROR_INTERNAL;
    }
    openFile(code);

    symbolTable = htabInit(SYMBOL_TABLE_SIZE);
    listInit(&instructionList);

    addPredefFunction(symbolTable, "length", TINTEGER, 1 * 2, "s", TSTRING);
    addPredefFunction(symbolTable, "copy", TSTRING, 3 * 2, "s", TSTRING, "i", TINTEGER, "n", TINTEGER);
    addPredefFunction(symbolTable, "find", TINTEGER, 2 * 2, "s", TSTRING, "search", TSTRING);
    addPredefFunction(symbolTable, "sort", TSTRING, 1 * 2, "s", TSTRING);

    result = parse(); //nesmi se dat return pred htabFree(),etc!!!!
    fclose(code);
    /*htabPrint(symbolTable);*/
    if (result != OK) {
        listDispose(&instructionList);
        htabFree(symbolTable);
        warning("Result: %d\n", result);
        return result;
    }


    tListOfInstr *headInstr = &instructionList;
    tInstr *firstInst =&(instructionList.first->instr);
    result = interpret(&firstInst, &instructionList);

    printf("Result: %d\n", result);
    //tiskni seznam, nebo ja uz nevim
    /*instructionList.act = instructionList.first;
    while (instructionList.act) {
        printf("polozka je typu %d\n", instructionList.act->instr.operation);
        instructionList.act = instructionList.act->nextItem;
    }
     */
    listDispose(headInstr);
    htabFree(symbolTable);
    freeNotFreeItems();
    //    addNotFreeTable(symbolTable);
    //    htab_t * dalsiTabulka;
    //    dalsiTabulka = htabInit(5);
    //    addNotFreeTable(dalsiTabulka);
    //    freeNotFreeTables();


    return result;

}

