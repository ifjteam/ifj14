#ifndef SCANNERSIM_H
#define	SCANNERSIM_H

typedef struct {
    int type;
    char* string;
} token_t; //struktura pro lexem



//terminaly
//DOLLAR urcuje rozsah tabulek a celkove urcite terminaly, musi zustat jako POSLEDNI TERMINAL
enum terminals {
    EQUAL = 0, NOTEQUAL, GREATER, GEQUAL, LESS, LEQUAL, PLUS, MINUS,
    TIMES, DIVISION, LBRACKET, RBRACKET, I, DOLLAR, E, EMPTY, SHIFT, REDUCT, NEXT,
    VAR, ID, COLON, COMMA, TBOOLEAN, TINTEGER, TREAL, TSTRING , SEMICOLON, BEGIN,
    ASSIGNMENT, BOOLEAN, INTEGER, REAL, STRING, END, DOT, FUNCTION, FORWARD, IF,
    FALSE, TRUE, THEN, ELSE, WHILE, DO, FILE_END
            
};

int getTokenSim(token_t *token);
#endif	/* SCANNERSIM_H */
