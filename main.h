/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */	
/*		Velecký Lukáš	xvelec05      								 */	
/*		Zemek Martin	xzemek04									 */	
/*																	 */
/*********************************************************************/

#ifndef MAIN_H
#define	MAIN_H

#define SYMBOL_TABLE_SIZE 50
#define LOCAL_TABLE_SIZE 20

enum terminals {
    EQUAL = 0, NOTEQUAL, GREATER, GEQUAL, LESS, LEQUAL, PLUS, MINUS,
    TIMES, DIVISION, LBRACKET, RBRACKET, I, DOLLAR, E, EMPTY, SHIFT, REDUCT, NEXT,
    VAR, ID, COLON, COMMA, TBOOLEAN, TINTEGER, TREAL, TSTRING , SEMICOLON, BEGIN,
    ASSIGNMENT, BOOLEAN, INTEGER, REAL, STRING, END, DOT, FUNCTION, FORWARD, IF,
    FALSE, TRUE, THEN, ELSE, WHILE, DO, WRITE, READLN, FILE_END            
};


#endif	/* MAIN_H */

