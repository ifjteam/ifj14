/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */	
/*		Velecký Lukáš	xvelec05      								 */	
/*		Zemek Martin	xzemek04									 */	
/*																	 */
/*********************************************************************/

#ifndef PSA_H
#define	PSA_H
#include "main.h"


//docasna funkce
void tabulkaMain(void);

//vytiskne globalni precedencni tabulku
void printPrecTab(void);

//ziska hodnotu z globalni precedencni tabulky
int getPrecTab(int radek, int sloupec);



extern int precedencniTabulka[(DOLLAR+1)*(DOLLAR+1)];

int findRule(int *rRule);
//alesova cast

//Structure for stack of integers
typedef struct nodeStruct{
        int data;
        struct nodeStruct *next;
}intStack_t;

/* Type of a stack object. */
typedef struct nodeStruct *intStack;

/* Destroy the stack object denoted by s.  This frees any dynamically-allocated
 *    memory associated with the stack. */
void intStackDispose(intStack *s);

/* Initialize a stack object. */
void intStackInit(intStack *s);

/* Push the integer val onto the stack. */
int intStackPush(intStack *s, int val);

/* Pop the integer val from the stack. */
int intStackPop(intStack *s);

//Find first terminal from enum(scanner/scannersim), if there is none returns -1
int intStackFirstTerminal(intStack *s);

int intStackPushBeforeFirstTerminal(intStack *s, int val);

#endif	/* PSA_H */

