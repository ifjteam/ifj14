/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */	
/*		Velecký Lukáš	xvelec05      								 */	
/*		Zemek Martin	xzemek04									 */	
/*																	 */
/*********************************************************************/

#include <stdio.h>
#include "main.h"
#include "psa.h"
#include "error.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



int precedencniTabulka[(DOLLAR + 1)*(DOLLAR + 1)] = {
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, REDUCT, SHIFT, REDUCT,
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, REDUCT, SHIFT, REDUCT,
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, REDUCT, SHIFT, REDUCT,
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, REDUCT, SHIFT, REDUCT,
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, REDUCT, SHIFT, REDUCT,
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, REDUCT, SHIFT, REDUCT,
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, SHIFT, SHIFT, SHIFT, REDUCT, SHIFT, REDUCT,
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, SHIFT, SHIFT, SHIFT, REDUCT, SHIFT, REDUCT,
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, SHIFT, REDUCT, SHIFT, REDUCT,
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, SHIFT, REDUCT, SHIFT, REDUCT,
    SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, NEXT, SHIFT, EMPTY,
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, EMPTY, REDUCT, EMPTY, REDUCT,
    REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, REDUCT, EMPTY, REDUCT, EMPTY, REDUCT,
    SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, SHIFT, EMPTY, SHIFT, EMPTY
};

void printPrecTab(void) {
    for (int i = 0; i <= DOLLAR; i++) {
        for (int j = 0; j <= DOLLAR; j++) {
            //tabulka[i*(DOLLAR+1)+j] = EMPTY;
            printf("%d:%d=%d\t", i, j, precedencniTabulka[i * (DOLLAR + 1) + j]);
        }
        printf("\n");
    }
}

int getPrecTab(int radek, int sloupec) {
    if ((radek >= EMPTY) || (sloupec >= EMPTY)) {
        return EMPTY;
    }
    return precedencniTabulka[radek * (DOLLAR + 1) + sloupec];
}

void tabulkaMain(void) {
    printPrecTab();
    printf("0:0=%d, 10:11=%d\n", getPrecTab(0, 0), getPrecTab(10, 11));
    //printf("\n velikost %d\n", sizeof(precedencniTabulka));


}

int findRule(int* rRule) {
    int ruleTable[13 * 3] = {
        E, EQUAL, E,
        E, NOTEQUAL, E,
        E, LESS, E,
        E, LEQUAL, E,
        E, GREATER, E,
        E, GEQUAL, E,
        E, PLUS, E,
        E, MINUS, E,
        E, TIMES, E,
        E, DIVISION, E,
        RBRACKET, E,LBRACKET, //zavorky jsou naopak, protoze psa pracuje se zasobnikem a obrati jejich poradi
        PLUS, E, -1,
        I, -1, -1
    };
    for (int j = 0; j < 13; j++){
        if (rRule[0] == ruleTable[3*j + 0]){
            if (rRule[1] == ruleTable[3*j + 1]){
                if (rRule[2] == ruleTable[3*j + 2]){
                    return j;
                }
            }
        }
    }
    return -1;
}

/* Initialize a stack object. */
void intStackInit(intStack *s) {
    *s = NULL;
    return;
}

int intStackPush(intStack *s, int value) {
    intStack_t *p;
    if ((p = malloc(sizeof (struct nodeStruct))) == NULL) {
        warning("internal: memory allocation failed for %d value in int_stack\n", value);
        return ERROR_INTERNAL;
    }

    p->data = value;
    p->next = *s;
    *s = p;

    return OK;
}

void intStackDispose(intStack *s) {
    intStack_t *p;

    while (*s) {
        p = *s;
        *s = p->next;
        free(p);
    }

    s = NULL;
    return;
}

int intStackFirstTerminal(intStack *s) {
    int result = -1;
    intStack_t *cPosition = *s;

    while (cPosition) {
        if (cPosition->data <= DOLLAR) {
            result = cPosition->data;
            break;
        }
        cPosition = cPosition->next;
        result = -1;
    }
    return result;

}

int intStackPushBeforeFirstTerminal(intStack *s, int value){
    intStack_t *p;
    if ((p = malloc(sizeof (struct nodeStruct))) == NULL) {
        warning("internal: memory allocation failed for %d value in int_stack\n", value);
        return ERROR_INTERNAL;
    }

    intStack_t *cPosition = *s;

    if ((*s)->data <= DOLLAR) {
        p->data = value;
        p->next = *s;
        *s = p;
    }
    else{
        while ((cPosition->next->data) > DOLLAR) {
            cPosition = cPosition->next;

            if (cPosition->next == NULL){
                warning("Na zasobniku, v psa, neni terminal\n");
                return -1;
            }
        }
        p->data = value;
        p->next = cPosition->next;
        cPosition->next = p;
    }

    return OK; 
}

int intStackPop(intStack *s) {
    int result;
    result = (*s)->data;
    intStack_t *toBePoped;
    toBePoped = *s;
    *s = toBePoped->next;
    free(toBePoped);
    return result;
}

