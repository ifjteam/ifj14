/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */	
/*		Velecký Lukáš	xvelec05      								 */	
/*		Zemek Martin	xzemek04									 */	
/*																	 */
/*********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "generator.h"
#include "error.h"
#include "scanner.h"

void listInit(tListOfInstr *L) {

    L->first = NULL;
    L->last = NULL;
    L->act = NULL;
}


void listDispose(tListOfInstr *L) {
    if (L == NULL) return;
    tListItem *ptr;
    while (L->first != NULL) {
        ptr = L->first;
        L->first = L->first->nextItem;
        free(ptr);
    }
}


void first(tListOfInstr *L) {
    L->act = L->first;
}


void succ(tListOfInstr *L) {
    if (L->act != NULL)
        L->act = L->act->nextItem;
}


tInstr *listGetData(tListOfInstr *L) {
    if (L->act == NULL) {
        return NULL;
        }
    else return &(L->act->instr);
}


void listJump(tListOfInstr *L, void *jumpInstr) {
    L->act = (tListItem*) jumpInstr;    
}


void *listGetPointerLast(tListOfInstr *L) {
    return (void*) L->last;
}


tListItem* listInsertLast(tListOfInstr *L, tInstr I) {
    tListItem *newItem;
    if ( (newItem = malloc(sizeof(struct listItem))) == NULL ){
        warning("%d:%d:Interni chyba pri alokaci pameti pro instrukci 3AC/n", row, character);
        return NULL;
    }
    newItem->instr = I;
    newItem->nextItem = NULL;
    if (L->first == NULL)
        L->first = newItem;
    else
        L->last->nextItem = newItem;
    L->last = newItem;

    return newItem;
}

//create & generate new instruction, then append it to listOfInstructions
tListItem* generateInstruction(int instType, void *addr1, void *addr2, void *addr3)
{
    tInstr instruction;
    instruction.operation = instType;
    instruction.op1 = addr1;
    instruction.op2 = addr2;
    instruction.result = addr3;

    tListItem* p;
    p = listInsertLast(&instructionList, instruction);

    if (p == NULL){
        return NULL;
    }
    return p;
}


void updateInstructionResult(tListItem* instruction ,void *addr3)
{
    instruction->instr.result = addr3;
}


