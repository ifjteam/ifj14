/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */	
/*		Velecký Lukáš	xvelec05      								 */	
/*		Zemek Martin	xzemek04									 */	
/*																	 */
/*********************************************************************/

#ifndef PARSER_H
#define	PARSER_H

#include "ial.h" //musi byt pro paramItem_t

#define getTokenMacro(token) if (getToken(&token) != 0) { \
        warning("%d:%d:lexical: line:%d: in function:%s\n", row, character, __LINE__,__func__); \
        return ERROR_LEXICAL;\
}

#define cmpTokenTypeMacro(token,Etype) if (token.type != Etype) { \
        warning("%d:%d:syntax: line:%d: in function:%s: type:%d: expected:%s - %d\n", row, character, __LINE__, __func__, token.type, stringify(Etype), Etype); \
        return ERROR_SYNTAX; \
}
 /*
#define getTokenMacro(token) if (getTokenSim(&token) != 0) { \
        warning("%d:%d:lexical: line:%d: in function:%s\n", 0, 0, __LINE__,__func__); \
        return ERROR_LEXICAL;\
}

#define cmpTokenTypeMacro(token,Etype) if (token.type != Etype) { \
        warning("%d:%d:syntax: line:%d: in function:%s: type:%d: expected:%s - %d\n", 0, 0, __LINE__, __func__, token.type, stringify(Etype), Etype); \
        return ERROR_SYNTAX; \
}*/

int parse(void);

//pravidla pro syntaktickou analyzu
int program(void);

int declarations(void);

int declrList(void);

int varDec(void);

int funcList(void);

int funcHeader(void);

int arguments(void);

int argumentList(void);

int arg(void);

int coumpndStat(void);

int statList(void);

int stat(void);

int assignStat(void);

int actuals(paramItem_t* paramsDefined,const char* key);

int paramList(void);

int ifStat(void);

int whileStat(void);

int expression(int type);

int simpleExpression(void);

int term(void);

int factor(void);

int checkAritmethicSemantics(int a, int b);

int checkRightToLeftSemantics(int a, int b);

/*  Nahraje ciselnou hodnotu/string do jedne polozky tabulky, kterou
 *  alokuje a vrati ukazatel na ni! Je potreba je posleze uvolnovat 
 *  v interpretu jakmile budou zbytecne na zasobniku.
 *  Pokud je na vstupu promenna vrati ukazatel na jeji polozku v 
 *  lokalni, pokud tam neni tak v, globalni tabulce symbolu.
 */ 
listitem_t* tokenToListitem_t(void);

/*  Funkce prijme hodnotu pouziteho pravidla 
 *  a podle typu pravidla vytvori instrukci pro
 *  interpret
 */
int ruleToInstructin(int usedRule);

/*  Najde funkce se jmenem id a nastavi jeji hodnotu
 *  ukazatele instruction na functionBegin, coz znaci, kde
 *  v seznamu instrukci zacina jeji kod.
 */
int updateFuncInstruction(char* id, tListItem* functionBegin);

int generateInstructionsToCopyParams(listitem_t* id); 

/*  Zpracuje parametry do podoby zkousnutelne interpretem
 */
listitem_t* parseParamsToListitems_t(void);

#endif	/* PARSER_H */

