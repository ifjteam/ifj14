/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */	
/*		Velecký Lukáš	xvelec05      								 */	
/*		Zemek Martin	xzemek04									 */	
/*																	 */
/*********************************************************************/

#ifndef IAL_H
#define	IAL_H

#include "generator.h"

//struktura pro seznam parametru funkce

typedef struct paramItem {
    unsigned char type;
    char * id;
    char* string;
    double value;
    struct paramItem *next;
} paramItem_t;

//funkce vklada parametr do seznamu parametru
int paramItemAdd(paramItem_t **list, char* id, unsigned char type);

//fce updatuje hodnotu, tj. real/boolean/int nebo string, sem se pripadne uklada ID promenne, aktualne posledniho parametru
int paramItemUpdate(paramItem_t **list, char* string, double value, char *id);

//fce uvolni seznam parametru
void paramItemFree(paramItem_t **list);


//struktura polozky seznamu hash tabulky

typedef struct htabListitem {
    const char* key;
    double value;
    char* string; //hodnota promenne typu string
    unsigned char type; //0-255 int,real,string
    unsigned char idType; // uninit,init,declared,defined
    paramItem_t *params;
    struct htab *local;
    struct htabListitem *next;
    tListItem* instructions;   //Funkce bude mit na teto adrese ulozeny odkaz na jeji prvni instrukci v 3AC
} listitem_t;

//enum pro htab_listitem.idtype, pozor stat v parseru pouziva >= DECLARED

enum {
    UNINIT = 0, INIT, DECLARED, DEFINED
};

//struktura hash tabulky

typedef struct htab {
    int htabSize;
    listitem_t **listArray;
} htab_t;
//tabulka symbolu pro program
extern htab_t* symbolTable;


//Structure for stack of tables

typedef struct nodeTableStruct {
    htab_t* table;
    struct nodeTableStruct *next;
} tableStack_t;

/* Type of a stack object. */
typedef struct nodeTableStruct *tableStack;

typedef struct tablesFree {
    htab_t* table;
    struct tablesFree *next;
} tablesToFree_t;

extern tablesToFree_t* notFreeTables;

typedef struct listOfItemsFree {
    listitem_t* item;
    struct listOfItemsFree *next;
} listOfItemsFree_t;

extern listOfItemsFree_t* notFreeItems;
/* Funkce pro rozdeleni slov do seznamu.
 *  @param str ukazatel na retezec, ktery se ma ulozit
 *  @param htab_size maximalni pocet seznamu  
 */
unsigned int hashFunction(const char *str, unsigned htab_size);

/* htabAdd  pro deklarovani(je jit vytvoreny newItem) ho zaradi do tabulky
 *          pro definovani(newItem je NULL) do newItem nastavi adresu
 *               jiz deklarovaneho id 
 *  @param table ukazatel na hash tabulku
 *  @param key ukazatel na retezec, ktery obsahuje klic
 *  @param ukazatel na novy/jiz deklarovany prvek(viz popis)
 * 
 *  @return ERROR_SEMANTIC pro pokus o redeklaraci/redefinici, jinak OK   
 */
int htabAdd(htab_t* table, const char* key, listitem_t *newItem);

/* Funkce pro inicializaci hashovaci tabulky.
 *  @param size velikost tabulky
 */
htab_t * htabInit(int size);

/* Funkce pro zkopirovani tabulky
 *  @param source - ukazatel na puvodni tabulku
 *  @return ukazatel na novou tabulku
 */
htab_t* copyLocalTable(htab_t *source);

int copyTableValues(htab_t *source, htab_t *dest);

/* Funkce length ze zadani
 *  @param string - vstupni string
 *  @return int delku retezce
 */
int length(char* string);

/* Funkce copy ze zadani
 *  @param string - vstupni string
 *  @param i - zacatek pozadovaneho retezce(pocitano od jednicky)
 *  @param n - delka podretezce
 *  @return podrezec
 */
char* copy(char* string, int index, int n);

/* Funkce find ze zadani(Boyer-Moore)
 *  @param string - vstupni string
 *  @param search - hledany podretezec
 *  @return vrati pozici(pocitano od 1),empty = 1, notfound = 0
 */
int find(char* string, char* search);

/* Funkce find ze zadani(Boyer-Moore)
 *  @param string - vstupni string
 *  @return vrati serazene znaky v stringu
 */
char* sort(char* string);

/* Pomocna funkce pro sort..
 */
char* merge(char* left, char* right);

/* Funkce prida zaznam o funkci do tabulky symbolu
 *  @param table - ukazatel na tabulku symbolu
 *  @param key - id funkce
 *  @param type - navratovy typ funkce
 *  @param argc - pocet nasledujicich parametru MUSI BYT nasobek 2 (2*k)
 *  @param id - musi byt char*
 *  @param type - musi byt char/int
 *  parametry id a type se musi dohromady vyskytnout prave ARGC-krat
 *  @return OK jinak error 
 */
int addPredefFunction(htab_t *table, const char *key, char type, const char argc, ...);

/* Funkce pro deklarovani promene 
 *  @param table - ukazatel na tabulku symbolu
 *  @param key - id promenne
 *  @param type - typ promenne
 *  @param initialized - (0-1)(UNINIT-INIT)
 *  @return OK jinak error
 */
int declareVar(htab_t *table, const char *key, char type, int initialized);

int assignVar(htab_t *table, const char *key, char type, double value, char* string);
/* Funkce deklaruje funkci do tabulky symbolu
 *  @param table ukazatel na hash tabulku
 *  @param key ukazatel na retezec, ktery obsahuje klic
 *  @param type typ navratove hodnoty
 *  @param params seznam parametru deklarovane funkce
 * 
 *  @return OK jinak error
 *  */
int declareFunc(htab_t *table, const char *key, char type, paramItem_t *params, htab_t** localTable);

//definuje funkci v tabulce symbolu
int defineFunc(htab_t *table, const char *key, char type, paramItem_t *params, htab_t** localTable);

/* Funkce vyhleda prvek podle klice key v tabulce a vrati na nej ukazatel. Pokud nenajde -> NULL
 * @param table ukazatel na hash tabulku
 * @param key ukazatel na retezec, ktery obsahuje klic
 */
listitem_t * htabLookup(htab_t *table, const char *key);

int htabPrint(htab_t* table);

/* Funkce vycisti obsah tabulky (a uvolni z pameti).
 * @param table ukazatel na hash tabulku
 */
int htabClear(htab_t *table);

/* Funkce uvolni celou tabulku.
 * @param table ukazatel na hash tabulku
 */
int htabFree(htab_t *table);

/* Funkce uchova seznam tabulek, ktere se pred koncem programu uvolni
 * @param table ukazatel na hash tabulku
 */
int addNotFreeTable(htab_t* table);

/* Funkce uchova seznam listItem_t, ktere se pred koncem programu uvolni
 * @param item ukazatel na listItem_t
 */
int addNotFreeItem(listitem_t* item);

int freeNotFreeItems(void);

//najde polozku a vrati jeji typ
unsigned char returnType(htab_t* table, htab_t* localTable, const char *key);

int freeNotFreeTables(void);
/* Destroy the stack object denoted by s.  This frees any dynamically-allocated
 *    memory associated with the stack. */
void tableStackDispose(tableStack *s);

/* Initialize a stack object. */
void tableStackInit(tableStack *s);

/* Push the integer val onto the stack. */
int tableStackPush(tableStack *s, htab_t *table);

/* Pop the integer val from the stack. */
void tableStackPop(tableStack *s);


#endif	/* IAL_H */

