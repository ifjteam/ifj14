/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */
/*		Velecký Lukáš	xvelec05      								 */
/*		Zemek Martin	xzemek04									 */
/*																	 */
/*********************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <ctype.h>
#include "interpret.h"
#include "generator.h"
#include "scanner.h"
#include "ial.h"
#include "error.h"
#include "main.h"


void stackInitV(stackV *s) {
    *s = NULL;
    return;
}


double stackTopV(stackV *s) {
    return (*s)->value;
}


void stackPopV(stackV *s) {

    *s = (*s)->next;
    return;
}


int stackPushV(stackV *s, double v) {
    stack_v *p;
    if ((p = malloc(sizeof (struct stackDatV))) == NULL) {
        warning("internal: memory allocation failed for %d value in stack\n", v);
        return ERROR_INTERNAL;
    }
    p->value = v;
    p->next = *s;
    *s = p;
    return OK;
}


void stackDisposeV(stackV *s) {
    stack_v *p;

    while (*s) {
        p = *s;
        *s = p->next;
        free(p);
    }
    s = NULL;
    return;
}


void stackInitS(stackS *s) {
    *s = NULL;
    return;
}


char* stackTopS(stackS *s) {

    return (*s)->string;
}


void stackPopS(stackS *s) {
    /*stackS *toBeDisposed = s;*/
    *s = (*s)->next;
    /*free ((*toBeDisposed)->string);*/
    /*free(*toBeDisposed);*/
    return;
}


int stackPushS(stackS *s, char* str) {
    stack_s *p;
    if ((p = malloc(sizeof (struct stackDatS))) == NULL) {
        warning("internal: memory allocation failed in stack\n");
        return ERROR_INTERNAL;
    }
    if ((p->string = malloc(sizeof (char) *(strlen(str) + 1))) == NULL) {
        warning("internal: memory allocation failed in stack\n");
        return ERROR_INTERNAL;
    }
    strcpy(p->string, str);
    p->next = *s;
    *s = p;
    return OK;
}


void stackDisposeS(stackS *s) {
    stack_s *p;

    while (*s) {
        p = *s;
        *s = p->next;
        free(p);
    }
    s = NULL;
    return;
}


void stackInitInt(stackInsInt *s) {
    *s = NULL;
    return;
}


tListItem* stackTopInt(stackInsInt *s) {

    return (*s)->ins;
}


void stackPopInt(stackInsInt *s) {

    *s = (*s)->next;
    return;
}


int stackPushInt(stackInsInt *s, tListItem* in) {
    stack_inst *p;
    if ((p = malloc(sizeof (struct stackInt))) == NULL) {
        warning("internal: memory allocation failed in stack\n");
        return ERROR_INTERNAL;
    }
    p->ins = in;
    p->next = *s;
    *s = p;
    return OK;
}


void stackDisposeInt(stackInsInt *s) {
    stack_inst *p;

    while (*s) {
        p = *s;
        *s = p->next;
        free(p);
    }
    s = NULL;
    return;
}


char* concatenate(char *str1, char *str2) {
    char *const string = malloc(strlen(str1) + strlen(str2) + 1);
    strcpy(string, str1);
    strcpy(string + strlen(str1), str2);
    return string;
}


int isInteger(char* cislo) {
    for (unsigned int i = 0; i < strlen(cislo); i++) {
        if (isdigit((unsigned)cislo[i]) == 0) {
            return 0;   //false - neni int
        }
    }
    return 1;   //true - je int
}


int isReal(char* cislo) {
    int realWithE = 0;
    int realWithSign = 0;
    int realWithDot = 0;
    char previousChar;
    if ((isdigit((unsigned)cislo[0]) == 0) || (isdigit((unsigned)cislo[strlen(cislo)-1]) == 0)) return 0; //false - neni real
    for (unsigned int i = 0; i < strlen(cislo); i++) {        
        if(cislo[i] == 'e' || cislo[i] == 'E')
        {
            if (realWithE == 1) return 0;   //false - neni real
            if (previousChar == '.') return 0;
            else realWithE = 1;
        }
        else if (cislo[i] == '.')
        {
            if (realWithDot == 1) return 0; //false - neni real
            else if (realWithE == 1) return 0;
            else realWithDot = 1;
        }
        else if (cislo[i] == '+' || cislo[i] == '-')
        {
            if (realWithSign == 1) return 0;    //false - neni real
            else if (!(previousChar == 'e' || previousChar == 'E')) return 0;  //false - neni real
            else realWithSign = 1;
        }
        else if (isdigit((unsigned)cislo[i]) == 0) return 0;    //false - neni real    
        previousChar = cislo[i];
    }
    if (realWithDot == 1 || realWithE == 1) return 1;
    else return 0;   //true - je real
}


int readInf(char** string) {
    int j = 0;
    if (((*string) = malloc(sizeof (char))) == NULL) {
        warning("internal: READLN: memory allocation failed for string from input\n");
        return ERROR_INTERNAL;
    }
    while (((*string)[j] = getchar()) != '\n') {
        if (((*string) = (char *) realloc((*string), sizeof (char) *(j + 2))) == NULL) {
            warning("internal: READLN: memory reallocation failed for string from input\n");
            return ERROR_INTERNAL;
        }
        j++;
    }
    (*string)[j] = '\0';
    return OK;
}



int interpret(tInstr **listOfInstructions, tListOfInstr *originalInstructionList) {

    int operation = 0;
    int i = 0;
    double val1, val2 = 0;
    char* str1;
    char* str2;   
    bool end = false;
    listitem_t *op1, *op2, *result;
    tListOfInstr *instructionList = originalInstructionList;
    
    tableStack zasobniktabulek;         //zásobník tabulek
    
    htab_t *tab_new;
    struct htab* tab;
    
    stackV stackV;                      //zásobník hodnot
    stackS stackS;                      //zásobník stringů
    stackInsInt stackInsInt;            //zásobník návratových hodnot
      
    first(instructionList);
    
    tableStackInit(&zasobniktabulek);
    stackInitV(&stackV);
    stackInitS(&stackS);
    stackInitInt(&stackInsInt);

    while (instructionList->act->instr.operation != I_START) {
        succ(instructionList);
    }
    succ(instructionList);

    if (*listOfInstructions == NULL) return OK;


    while (end == false) {

        i++;                            //počet instrukcí
        operation = instructionList->act->instr.operation;
        op1 = instructionList->act->instr.op1;
        op2 = instructionList->act->instr.op2;
        result = instructionList->act->instr.result;

        switch (operation) {

            case I_ASSIGN:
                if (op1 == NULL) {
                    if (result->type == TSTRING) {
                        if (stackS) {
                            str1 = stackTopS(&stackS);
                            stackPopS(&stackS);
                        }
                        result->string = str1;
                        result->idType = INIT;
                    }
                    else if ((result->type == TINTEGER) || (result->type == TREAL) || (result->type == TBOOLEAN)) { 
                        if (stackV) {
                            val1 = stackTopV(&stackV);
                            stackPopV(&stackV);
                        }
                        result->value = val1;
                        result->idType = INIT;
                    }
                    else { 
                        warning("Interpret: illegal operation\n");
                        return ERROR_RUN_OTHER;
                    }
                }
                else {
                    if ((op1->idType) == INIT) {
                        
                        if (op1->type == TREAL) {

                            result->value = op1->value;
                            result->type = TREAL;
                            result->idType = INIT;
                        }
                        else if (op1->type == TINTEGER) {

                            result->value = op1->value;
                            result->type = TINTEGER;
                            result->idType = INIT;
                        }
                        else if (op1->type == TBOOLEAN) {

                            result->value = op1->value;
                            result->type = TBOOLEAN;
                            result->idType = INIT;
                        }
                        else if (op1->type == TSTRING) {

                            result->value = op1->value;
                            result->type = TSTRING;
                            result->idType = INIT;
                        }
                        else {
                            warning("Interpret: illegal operation\n");
                            return ERROR_RUN_OTHER;
                        }
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
                }
            break;



            case I_ADD:
                if (op1 == NULL) {  
                    if (stackV) {

                        val1 = stackTopV(&stackV);
                        stackPopV(&stackV);

                        if (stackV) {

                            val2 = stackTopV(&stackV);
                            stackPopV(&stackV);
                        }
                        stackPushV(&stackV, val2 + val1);
                    }

                    if (stackS) {
                        str1 = stackTopS(&stackS);
                        stackPopS(&stackS);

                        if (stackS) {
                            str2 = stackTopS(&stackS);
                            stackPopS(&stackS);
                        }                        
                        stackPushS(&stackS, concatenate(str2, str1));
                    }
                }
                else {
                    if (((op1->idType) == INIT) && ((op2->idType) == INIT)) { 
                                           
                        if ((op1->type == TREAL) || (op2->type == TREAL)) {

                            if ((op1->type == TREAL) && (op2->type == TREAL)) {

                                result->value = (op1->value) + (op2->value);
                                result->type = TREAL;
                            }
                            else if ((op1->type == TINTEGER) || (op2->type == TINTEGER)) {

                                result->value = (op1->value) + (op2->value);
                                result->type = TREAL;
                            }
                            else {
                                warning("Interpret: illegal operation\n");
                                return ERROR_RUN_OTHER;
                            }
                        }
                        else if ((op1->type == TINTEGER) && (op2->type == TINTEGER)) {

                            result->value = (op1->value) + (op2->value);
                            result->type = TINTEGER;
                        }
                        else if ((op1->type == TSTRING) && (op2->type == TSTRING)) {

                            result->string = concatenate(op1->string, op2->string);
                            result->type = TSTRING;
                        }
                        else {
                            warning("Interpret: illegal operation\n");
                            return ERROR_RUN_OTHER;
                        }
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
                }
            break;



            case I_SUB:
                if (op1 == NULL) {

                    if (stackV) {

                        val1 = stackTopV(&stackV);
                        stackPopV(&stackV);

                        if (stackV) {

                            val2 = stackTopV(&stackV);
                            stackPopV(&stackV);
                        }
                    }
                    stackPushV(&stackV, val2 - val1);
                }
                else {
                    if (((op1->idType) == INIT) && ((op2->idType) == INIT)) {
                        
                        if ((op1->type == TREAL) || (op2->type == TREAL)) {

                            if ((op1->type == TREAL) && (op2->type == TREAL)) {

                                result->value = (op1->value) - (op2->value);
                                result->type = TREAL;
                            }
                            else if ((op1->type == TINTEGER) || (op2->type == TINTEGER)) {

                                result->value = (op1->value) - (op2->value);
                                result->type = TREAL;
                            }
                            else {
                                warning("Interpret: illegal operation\n");
                                return ERROR_RUN_OTHER;
                            }
                        }
                        else if ((op1->type == TINTEGER) && (op2->type == TINTEGER)) {

                            result->value = (op1->value) - (op2->value);
                            result->type = TINTEGER;
                        }
                        else {
                            warning("Interpret: illegal operation\n");
                            return ERROR_RUN_OTHER;
                        }
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
                }
            break;



            case I_MUL:
                if (op1 == NULL) {                        

                    if (stackV) {

                        val1 = stackTopV(&stackV);
                        stackPopV(&stackV);

                        if (stackV) {

                            val2 = stackTopV(&stackV);
                            stackPopV(&stackV);
                        }
                    }
                    stackPushV(&stackV, val2 * val1);
                }
                else {
                    if (((op1->idType) == INIT) && ((op2->idType) == INIT)) {
                        
                        if ((op1->type == TREAL) || (op2->type == TREAL)) {

                            if ((op1->type == TREAL) && (op2->type == TREAL)) {

                                result->value = (op1->value) * (op2->value);
                                result->type = TREAL;
                            }
                            else if ((op1->type == TINTEGER) || (op2->type == TINTEGER)) {

                                result->value = (op1->value) * (op2->value);
                                result->type = TREAL;
                            }
                            else {
                                warning("Interpret: illegal operation\n");
                                return ERROR_RUN_OTHER;
                            }
                        }
                        else if ((op1->type == TINTEGER) && (op2->type == TINTEGER)) {

                            result->value = (op1->value) * (op2->value);
                            result->type = TINTEGER;
                        }
                        else {
                            warning("Interpret: illegal operation\n");
                            return ERROR_RUN_OTHER;
                        }
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
                }
            break;



            case I_DIV:
                if (op1 == NULL) {                        

                    if (stackV) {

                        val1 = stackTopV(&stackV);
                        stackPopV(&stackV);

                        if (stackV) {

                            val2 = stackTopV(&stackV);
                            stackPopV(&stackV);

                            if ((val1 == 0) || (val1 == 0.0)) {
                                stackDisposeV(&stackV);
                                stackDisposeS(&stackS);
                                warning("Division by zero\n");
                                return ERROR_RUN_DIV_BY_ZERO;
                            } 
                        }
                    }
                    double num = val2 / val1;
                    stackPushV(&stackV, num);
                }
                else {
                    if (((op1->idType) == INIT) && ((op2->idType) == INIT)) {
                        
                        if ((op1->type == TREAL) || (op2->type == TREAL)) {

                            if ((op1->type == TREAL) && (op2->type == TREAL)) {

                                if ((op2->value != 0.0) || (op2->value != 0)) {
                                    result->value = (op1->value) / (op2->value);
                                    result->type = TREAL;
                                }
                                else {
                                    warning("Division by zero\n");
                                    return ERROR_RUN_DIV_BY_ZERO;
                                }
                            }
                            else if ((op1->type == TINTEGER) || (op2->type == TINTEGER)) {

                                if ((op2->value != 0.0) || (op2->value != 0)) {
                                    result->value = (op1->value) / (op2->value);
                                    result->type = TREAL;
                                }
                                else {
                                    warning("Division by zero\n");
                                    return ERROR_RUN_DIV_BY_ZERO;
                                }
                            }
                            else {
                                warning("Interpret: illegal operation\n");
                                return ERROR_RUN_OTHER;
                            }
                        }
                        else if ((op1->type == TINTEGER) && (op2->type == TINTEGER)) {

                            if (op2->value != 0) {
                                result->value = (op1->value) / (op2->value);
                                result->type = TREAL;
                            }
                            else {
                                warning("Division by zero\n");
                                return ERROR_RUN_DIV_BY_ZERO;                              
                            }
                        }
                        else {
                            warning("Interpret: illegal operation\n");
                            return ERROR_RUN_OTHER;
                        }
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
                }
            break;

            case I_EQUAL:
                if (op1 == NULL) {

                    if (stackV) {                            
                        val1 = stackTopV(&stackV);
                        stackPopV(&stackV);

                        if (stackV) {
                            val2 = stackTopV(&stackV);
                            stackPopV(&stackV);
                        }
                        if (val2 == val1) stackPushV(&stackV, 1);
                        else stackPushV(&stackV, 0);
                    }


                    if (stackS) {
                        str1 = stackTopS(&stackS);
                        stackPopS(&stackS);

                        if (stackS) {
                            str2 = stackTopS(&stackS);
                            stackPopS(&stackS);
                        }
                        if (strcmp(str2, str1) == 0 ) stackPushV(&stackV, 1);
                        else stackPushV(&stackV, 0);
                    }

                }
                else {
                    if (((op1->idType) == INIT) && ((op2->idType) == INIT)) {
                        
                        if ((op1->type == TREAL) && (op2->type == TREAL)) {
                            
                            if (op1->value == op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TINTEGER) && (op2->type == TINTEGER)) {

                            if (op1->value == op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TBOOLEAN) && (op2->type == TBOOLEAN)) {

                            if (op1->value == op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TSTRING) && (op2->type == TSTRING)) {

                            if (strcmp(op1->string, op2->string) == 0 ) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else {
                            warning("Interpret: illegal operation\n");
                            return ERROR_RUN_OTHER;
                        }
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
                }
            break;


            case I_NEQUAL:
                if (op1 == NULL) {

                    if (stackV) {                            
                        val1 = stackTopV(&stackV);
                        stackPopV(&stackV);

                        if (stackV) {
                            val2 = stackTopV(&stackV);
                            stackPopV(&stackV);
                        }
                        if (val2 != val1) stackPushV(&stackV, 1);
                        else stackPushV(&stackV, 0);
                    }

                    if (stackS) {
                        str1 = stackTopS(&stackS);
                        stackPopS(&stackS);

                        if (stackS) {
                            str2 = stackTopS(&stackS);
                            stackPopS(&stackS);    
                        }
                        if (strcmp(str2, str1) != 0 ) stackPushV(&stackV, 1);
                        else stackPushV(&stackV, 0); 
                    }                                                                              
                }
                else {
                    if (((op1->idType) == INIT) && ((op2->idType) == INIT)) {
                                                
                        if ((op1->type == TREAL) && (op2->type == TREAL)) {
                            
                            if (op1->value != op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TINTEGER) && (op2->type == TINTEGER)) {

                            if (op1->value != op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TBOOLEAN) && (op2->type == TBOOLEAN)) {

                            if (op1->value != op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TSTRING) && (op2->type == TSTRING)) {

                            if (strcmp(op1->string, op2->string) != 0 ) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else {
                            warning("Interpret: illegal operation\n");
                            return ERROR_RUN_OTHER;
                        }
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
                }
            break;



            case I_LEQUAL:
                if (op1 == NULL) {

                    if (stackV) {                            
                        val1 = stackTopV(&stackV);
                        stackPopV(&stackV);

                        if (stackV) {
                            val2 = stackTopV(&stackV);
                            stackPopV(&stackV);
                        }
                        if (val2 <= val1) stackPushV(&stackV, 1);
                        else stackPushV(&stackV, 0);
                    }

                    if (stackS) {
                        str1 = stackTopS(&stackS);
                        stackPopS(&stackS);

                        if (stackS) {
                            str2 = stackTopS(&stackS);
                            stackPopS(&stackS);  
                        }
                        if ((strcmp(str2, str1) == 0 ) || (strlen(str2) < strlen(str1))) stackPushV(&stackV, 1);
                        else stackPushV(&stackV, 0);
                    } 
                }
                else {
                    if (((op1->idType) == INIT) && ((op2->idType) == INIT)) { 
                                               
                        if ((op1->type == TREAL) && (op2->type == TREAL)) {
                            
                            if (op1->value <= op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TINTEGER) && (op2->type == TINTEGER)) {

                            if (op1->value <= op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TBOOLEAN) && (op2->type == TBOOLEAN)) {

                            if (op1->value <= op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TSTRING) && (op2->type == TSTRING)) {

                            if ((strcmp(op1->string, op2->string) == 0 ) || (strlen(op1->string) < strlen(op2->string))) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else {
                            
                            warning("Interpret: illegal operation\n");
                            return ERROR_RUN_OTHER;
                        }
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
                }
            break;



            case I_GEQUAL:
                if (op1 == NULL) {                                            

                    if (stackV) {                            
                        val1 = stackTopV(&stackV);
                        stackPopV(&stackV);

                        if (stackV) {
                            val2 = stackTopV(&stackV);
                            stackPopV(&stackV);
                        }
                        if (val2 >= val1) stackPushV(&stackV, 1);
                        else stackPushV(&stackV, 0);
                    }

                    if (stackS) {
                        str1 = stackTopS(&stackS);
                        stackPopS(&stackS);

                        if (stackS) {
                            str2 = stackTopS(&stackS);
                            stackPopS(&stackS); 
                        }
                        if ((strcmp(str2, str1) == 0 ) || (strlen(str2) > strlen(str1))) stackPushV(&stackV, 1);
                        else stackPushV(&stackV, 0);
                    } 
                }
                else {
                    if (((op1->idType) == INIT) && ((op2->idType) == INIT)) {  
                                              
                        if ((op1->type == TREAL) && (op2->type == TREAL)) {
                            
                            if (op1->value >= op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TINTEGER) && (op2->type == TINTEGER)) {

                            if (op1->value >= op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TBOOLEAN) && (op2->type == TBOOLEAN)) {

                            if (op1->value >= op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TSTRING) && (op2->type == TSTRING)) {

                            if ((strcmp(op1->string, op2->string) == 0 ) || (strlen(op1->string) > strlen(op2->string))) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else {
                            warning("Interpret: illegal operation\n");
                            return ERROR_RUN_OTHER;
                        }
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
                }
            break;



            case I_LESS:
                if (op1 == NULL) {

                        if (stackV) {                            
                            val1 = stackTopV(&stackV);
                            stackPopV(&stackV);

                            if (stackV) {
                                val2 = stackTopV(&stackV);
                                stackPopV(&stackV);
                            }
                            if (val2 < val1) stackPushV(&stackV, 1);
                            else stackPushV(&stackV, 0);
                        }

                        if (stackS) {
                            str1 = stackTopS(&stackS);
                            stackPopS(&stackS);

                            if (stackS) {
                                str2 = stackTopS(&stackS);
                                stackPopS(&stackS);    
                            }
                            if (strlen(str2) < strlen(str1)) stackPushV(&stackV, 1);
                            else stackPushV(&stackV, 0);
                        }
                    }                                                              
                else {
                    if (((op1->idType) == INIT) && ((op2->idType) == INIT)) { 
                                               
                        if ((op1->type == TREAL) && (op2->type == TREAL)) {
                            
                            if (op1->value < op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TINTEGER) && (op2->type == TINTEGER)) {

                            if (op1->value < op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TBOOLEAN) && (op2->type == TBOOLEAN)) {

                            if (op1->value < op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TSTRING) && (op2->type == TSTRING)) {

                            if (strlen(op1->string) < strlen(op2->string)) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else {
                            warning("Interpret: illegal operation\n");
                            return ERROR_RUN_OTHER;
                        }
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
                }
            break;


            case I_GREATER:
                if (op1 == NULL) {

                    if (stackV) {                            
                        val1 = stackTopV(&stackV);
                        stackPopV(&stackV);

                        if (stackV) {
                            val2 = stackTopV(&stackV);
                            stackPopV(&stackV);
                        }
                        if (val2 > val1) stackPushV(&stackV, 1);
                        else stackPushV(&stackV, 0);
                    }

                    if (stackS) {
                        str1 = stackTopS(&stackS);
                        stackPopS(&stackS);

                        if (stackS) {
                            str2 = stackTopS(&stackS);
                            stackPopS(&stackS);
                        }
                        if (strlen(str2) > strlen(str1)) stackPushV(&stackV, 1);
                        else stackPushV(&stackV, 0);
                    }
                }                                                           
                else {  
                    if (((op1->idType) == INIT) && ((op2->idType) == INIT)) {
                                              
                        if ((op1->type == TREAL) && (op2->type == TREAL)) {
                            
                            if (op1->value > op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TINTEGER) && (op2->type == TINTEGER)) {

                            if (op1->value > op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TBOOLEAN) && (op2->type == TBOOLEAN)) {

                            if (op1->value > op2->value) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else if ((op1->type == TSTRING) && (op2->type == TSTRING)) {

                            if (strlen(op1->string) > strlen(op2->string)) {
                                result->value = 1;
                                result->type = TBOOLEAN;
                            }
                            else {
                                result->value = 0;
                                result->type = TBOOLEAN;
                            }
                        }
                        else {
                            warning("Interpret: illegal operation\n");
                            return ERROR_RUN_OTHER;
                        }
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
                }
            break;



            case I_WRITE: 
                if (op1->idType == INIT) {
                    if (op1->type == TREAL) {

                        if (op1->idType == INIT) {
                            printf("%g", op1->value);
                        }
                        else {
                            warning("Interpret uninitialized: uninitialized variable\n");
                            return ERROR_RUN_UNINITIALIZED;
                        }
                    }
                    else if (op1->type == TINTEGER) {

                        int num = (int) op1->value;
                        printf("%d", num);
                    }
                    else if (op1->type == TSTRING) {
                        
                        printf("%s", op1->string);
                    }
                    else if (op1->type == TBOOLEAN) {
                  
                        int num = (int) op1->value;
                        printf("%d", num);
                    }
                    else {
                        warning("Interpret: illegal operation\n");
                        return ERROR_RUN_OTHER;
                    }
                }
                else {
                    warning("Interpret uninitialized: uninitialized variable\n");
                    return ERROR_RUN_UNINITIALIZED;
                }
            break;



            case I_READ:
                if (result->type == TREAL) {
                   /* if (scanf("%g", &result->value) != 1) {
                        return ERROR_RUN_INPUT;
                        }*/
                    char* cislo;
                    int vysledek;
                    if ((vysledek = readInf(&cislo)) != OK) {
                        return vysledek;
                    }

                    double num;
                    if (isReal(cislo)) {
                        num = atof(cislo);
                        result->value = num;
                        result->idType = INIT;
                    }
                    else {
                        warning("Interpret input: input read real value error\n");
                        return ERROR_RUN_INPUT;
                    }
                }
                else if (result->type == TINTEGER) {
                    /*int num;
                    if (scanf("%d", &num) != 1) {
                        return ERROR_RUN_INPUT;
                    }
                    result->value = num;
                    getchar();*/
                    char* cislo;
                    int vysledek;
                    if ((vysledek = readInf(&cislo)) != OK) {
                        return vysledek;
                    }

                    int num;
                    if (isInteger(cislo)) {
                        num = atoi(cislo);
                        result->value = num;
                        result->idType = INIT;
                    }
                    else {
                        warning("Interpret input: input read integer value error\n");
                        return ERROR_RUN_INPUT;
                    }
                }
                else if (result->type == TSTRING) {
                    /*if (scanf("%s", result->string) != 1) {
                        return ERROR_RUN_INPUT;
                    }
                    getchar();
                     */
                    int vysledek;
                    char* string;
                    if ((vysledek = readInf(&string)) != OK) {
                        return vysledek;
                    }
                    if (result->string != NULL) {
                        free((char *) result->string);      //uvolneni stareho
                    }
                    result->string = string;
                    result->idType = INIT;
                }
                else {
                    warning("Interpret input: input read string error\n");
                    return ERROR_RUN_OTHER;
                }
            break;



            case I_J:
                if (op1 != NULL) {
                    if ((strcmp(op1->key, "length") == 0 )) {
                        stackPushV(&stackV, length(op2->string));
                    }
                    else if ((strcmp(op1->key, "copy") == 0 )) {
                        val1 = stackTopV(&stackV);
                        stackPopV(&stackV);
                        stackPushS(&stackS, copy(op2->string, result->value, val1));
                    }
                    else if ((strcmp(op1->key, "find") == 0 )) {
                        stackPushV(&stackV, find(op2->string, result->string));
                    }
                    else if ((strcmp(op1->key, "sort") == 0 )) {
                        stackPushS(&stackS, sort(op2->string));
                    }
                    else {
                        stackPushInt(&stackInsInt, instructionList->act);
                        listJump(instructionList, result);
                    }
                }
                else listJump(instructionList, result);
            break;


            case I_JZERO:
                    if (stackTopV(&stackV) == 0) {
                        listJump(instructionList, result);
                    }
            break;



            case I_PUSH:
                    if ((op1->idType) == INIT) {                
                        if ((op1->type) == TSTRING) stackPushS(&stackS, op1->string);
                        else stackPushV(&stackV, op1->value);
                    }
                    else {
                        warning("Interpret uninitialized: uninitialized variable\n");
                        return ERROR_RUN_UNINITIALIZED;
                    }
            break;


            case I_FCE_END:
                if (op1->type == STRING) {
                    stackPushS(&stackS, op1->string);
                }
                else {
                    stackPushV(&stackV, op1->value);
                }
                tab = result->local;
                copyTableValues(zasobniktabulek->table, tab);
                tableStackPop(&zasobniktabulek);            //uvolni vyhodi tabulku ze zasobniku a uvolni ji

                instructionList->act = stackTopInt(&stackInsInt);
                stackPopInt(&stackInsInt);

                /*tab = copyLocalTable(zasobniktabulek->table);*/
            break;


            case I_FCE_START:
                tab = op1->local;
                tab_new=copyLocalTable(op1->local);
                tableStackPush(&zasobniktabulek, tab_new);   //pushneme tabulku nazasobnik
            break;


            case I_START:
            break;

            
            case  I_LABEL:
            break;

            
            case I_END:
                end = true;
            break;
        }
        succ(instructionList);   
    }
    stackDisposeV(&stackV);
    stackDisposeS(&stackS);
    stackDisposeInt(&stackInsInt);
    listDispose(instructionList);
    return OK;
}