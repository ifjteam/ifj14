/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */	
/*		Velecký Lukáš	xvelec05      								 */	
/*		Zemek Martin	xzemek04									 */	
/*																	 */
/*********************************************************************/

#ifndef GENERATOR_H
#define	GENERATOR_H

typedef struct {
    int operation;
    void *op1;
    void *op2;
    void *result;
} tInstr;


typedef struct listItem {
  tInstr instr;
  struct listItem *nextItem;
} tListItem;


typedef struct {
  struct listItem *first;
  struct listItem *last;
  struct listItem *act;
} tListOfInstr;

extern tListOfInstr instructionList; 

enum operation {
    I_ASSIGN,
    I_ADD,
    I_SUB,
    I_MUL,
    I_DIV,
    I_EQUAL,
    I_NEQUAL,
    I_LEQUAL,
    I_GEQUAL,
    I_LESS,
    I_GREATER,
    I_WRITE,
    I_READ,
    I_LABEL,
    I_END,
    I_START,
    I_JZERO,
    I_J,
    I_PUSH,
    I_FCE_END,
    I_FCE_START
};


void listInit(tListOfInstr *L);
void listDispose(tListOfInstr *L);
void first(tListOfInstr *L);
void succ(tListOfInstr *L);
tInstr *listGetData(tListOfInstr *L);
void listJump(tListOfInstr *L, void *jumpInstr);
void *listGetPointerLast(tListOfInstr *L);
tListItem* listInsertLast(tListOfInstr *L, tInstr I);


//generuje instrukci a vlozi ji na konec zadaneho seznamu
tListItem* generateInstruction(int instType, void *addr1, void *addr2, void *addr3);

//upravi cilovy obsah, je potreba pro podminene skoky na navesti
void updateInstructionResult(tListItem* instruction ,void *addr3);


#endif	/* GENERATOR_H */
