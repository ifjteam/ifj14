/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */	
/*		Velecký Lukáš	xvelec05      								 */	
/*		Zemek Martin	xzemek04									 */	
/*																	 */
/*********************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "error.h"

void warning(const char *fmt, ...){
    va_list args;
    va_start (args, fmt);
    fprintf(stderr, "CHYBA: ");
    vfprintf(stderr, fmt, args);
    va_end (args);
 
}
