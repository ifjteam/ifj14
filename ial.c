/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */
/*		Velecký Lukáš	xvelec05      								 */
/*		Zemek Martin	xzemek04									 */
/*																	 */
/*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdarg.h>
#include "ial.h"
#include "error.h"
#include "scanner.h"
#include "main.h"

unsigned int hashFunction(const char *str, unsigned htab_size) {
    unsigned int h = 0;
    const unsigned char *p;
    for (p = (const unsigned char*) str; *p != '\0'; p++)
        h = 65599 * h + *p;
    return h % htab_size;
}

htab_t * htabInit(int size) {
    htab_t *htab;
    if ((htab = malloc(sizeof (htab_t))) == NULL) return NULL;
    htab->htabSize = size;
    if ((htab->listArray = malloc(size * sizeof (listitem_t))) == NULL) return NULL;
    for (int i = 0; i < size; i++) {
        htab->listArray[i] = NULL;
    }
    return htab;
}

htab_t* copyLocalTable(struct htab* source) {
    htab_t* newTable;
    listitem_t *item;
    listitem_t *newItem = NULL;
    listitem_t *posledni;
    if (source == NULL) {
        warning("internal: copyLocalTable: source = NULL pointer\n");
        return NULL;
    }
    newTable = htabInit(source->htabSize);
    for (int j = 0; j < source->htabSize; j++) {
        item = source->listArray[j];
        posledni = NULL;
        newItem = NULL;
        while (item != NULL) {
            if (item->idType >= DECLARED) {
                warning("internal: copyLocalTable: copy of Functions(%s) not supported(didn't copy)\n", item->key);
            }

            if ((newItem = malloc(sizeof (listitem_t))) == NULL) {
                warning("internal: copyLocalTable: memory allocation failed for copy of%s\n", item->key);
                return NULL;
            }
            char* pole;
            if ((pole = malloc(strlen(item->key) + 1)) == NULL) {
                warning("internal: copyLocalTable: memory allocation failed for copy of %s\n", item->key);
                return NULL;
            }
            strcpy(pole, item->key);
            newItem->key = pole;
            newItem->type = item->type;
            newItem->idType = item->idType;
            newItem->value = item->value;
            newItem->params = NULL;
            newItem->local = NULL;
            newItem->instructions = NULL;
            if (item->string != NULL) {
                if ((pole = malloc(strlen(item->string) + 1)) == NULL) {
                    warning("internal: memory allocation failed for copy of %s\n", item->key);
                    return NULL;
                }
                strcpy(pole, item->string);
                newItem->string = pole;
            } else {
                newItem->string = NULL;
            }
            newItem->next = NULL;
            if (posledni != NULL) {
                posledni->next = newItem;
            } else { //prvni
                newTable->listArray[j] = newItem;
            }
            posledni = newItem;
            item = item->next;
        }//konec while

    }
    return newTable;
}

int copyTableValues(htab_t* source, htab_t* dest) {
    listitem_t *sourceItem;
    listitem_t *destItem;
    if (source == NULL) {
        warning("internal: copyTableValues: source = NULL pointer\n");
        return ERROR_INTERNAL;
    }
    if (dest == NULL) {
        warning("internal: copyTableValues: dest = NULL pointer\n");
        return ERROR_INTERNAL;
    }
    if (source->htabSize != dest->htabSize) {
        warning("internal: copyTableValues: different size of tables\n");
        return ERROR_INTERNAL;
    }
    for (int j = 0; j < source->htabSize; j++) {
        sourceItem = source->listArray[j];
        destItem = dest->listArray[j];
        while (sourceItem != NULL && destItem != NULL) {
            if (strcmp(sourceItem->key, destItem->key) != 0) {
                warning("internal: copyTableValues: item keys do not match\n");
                return ERROR_INTERNAL;
            }
            destItem->idType = sourceItem->idType;
            destItem->value = sourceItem->value;
            char* pole;
            if (sourceItem->string != NULL) {
                if ((pole = malloc(strlen(sourceItem->string) + 1)) == NULL) {
                    warning("internal: copyTableValues: memory allocation failed for copy of %s\n", sourceItem->key);
                    return ERROR_INTERNAL;
                }
                strcpy(pole, sourceItem->string);
                free((char*) destItem->string); //uvolneni toho, co budem prepisovat
                destItem->string = pole;
            } else {
                destItem->string = NULL;
            }


            sourceItem = sourceItem->next;
            destItem = destItem->next;
        }
        if (sourceItem != NULL || destItem != NULL) {
            warning("internal: copyTableValues: number of items do not match\n");
            return ERROR_INTERNAL;
        }


    }
    return OK;
}

listitem_t * htabLookup(htab_t *table, const char *key) {
    if (table == NULL) {
        fprintf(stderr, "%d:%d:Internal:htabLookup:Tabulka neni inicializovana(klic:%s)\n", row, character, key);
        return NULL;
    }
    if (strcmp(key, "") == 0) {
        fprintf(stderr, "Zadany retezec je prazdny\n");
        return NULL;
    }
    listitem_t *item;
    int index = hashFunction(key, table->htabSize);
    item = table->listArray[index];

    while (item != NULL) {
        if (strcmp(item->key, key) == 0) {
            //nasel 
            return item;
        }
        item = item->next;
    }

    return NULL;
}

int htabPrint(htab_t* table) {
    listitem_t *item;
    if (table == NULL) {
        warning("internal: htabPrint: table = NULL pointer");
        return ERROR_INTERNAL;
    }
    for (int j = 0; j < table->htabSize; j++) {

        item = table->listArray[j];
        while (item != NULL) {
            printf("tab[%d] = %s\t idtype:%d, type:%d\n", j, item->key, item->idType, item->type);
            item = item->next;
        }

    }
    return OK;
}

int htabClear(htab_t* table) {
    listitem_t *item;
    listitem_t *tmp;
    paramItem_t *param;
    paramItem_t *tmpParam;
    if (table == NULL) {
        fprintf(stderr, "%d:%d:Internal:htabClear:Tabulka neni inicializovana\n", row, character);
        return ERROR_INTERNAL;
    }
    for (int j = 0; j < table->htabSize; j++) {
        item = table->listArray[j];
        while (item != NULL) {
            tmp = item;
            //printf("list[%d]= %s : %d\n", j, tmp->key, tmp->data);
            if (tmp->key != NULL) {
                free((char*) tmp->key);
                item->key = NULL;
            }
            if (tmp->string != NULL) {
                free((char*) tmp->string);
                item->string = NULL;
            }
            //uvolneni seznamu parametru
            param = tmp->params;
            while (param != NULL) {
                tmpParam = param;
                param = tmpParam->next;
                if (tmpParam->id != NULL) {
                    /*free(tmpParam->id);*/
                }
                free(tmpParam);
            }
            if (tmp->local != NULL) {
                htabFree(tmp->local);
                item->local = NULL;
            }
            if (tmp->instructions != NULL) {
                free(tmp->instructions);
            }
            item = item->next;
            free(tmp);
        }
        table->listArray[j] = NULL;
    }
    return 0;
}

int htabFree(htab_t* table) {
    htabClear(table);
    free(table->listArray);
    free(table);
    return 0;
}

//funkce pro pascal

int length(char* string) {
    if (string != NULL) {
        return strlen(string);
    } else {
        return 0;
    }
}

char* copy(char* string, int index, int n) {
    char* result;
    int delka = strlen(string);
    if (index > delka) {
        return ""; //prazdny string
    }
    if (index < 1) {
        warning("Runtime:Copy:Index must be bigger than zero!(not: %d)\n", index);
        return NULL;
    }
    if (n < 0) {
        warning("Runtime:Copy:Index must be equal or bigger than zero!(not: %d)\n", n);
        return NULL;
    }
    if (delka - index < n) { //n je delsi nez retezec
        n = delka - (index - 1);
    }
    if ((result = malloc(sizeof (char)*(n + 1))) == NULL) {
        warning("Runtime:Copy:Not enough memory for substring of: %s\n", string);
        return NULL;
    }
    for (int j = 0; j < n; j++) {
        result[j] = string[index + j - 1]; //-1 protoze index je od prvniho prvku ne od nuly
    }
    result[n] = '\0';

    return result;
}

int find(char* string, char* search) {
    int subLength = strlen(search);
    if (subLength == 0) { //prazdny podretezec
        return 1;
    }

    int jump[UCHAR_MAX];
    //compute jump
    for (int i = 0; i <= UCHAR_MAX; i++) { //pro vetsinu
        jump[i] = subLength;
    }
    for (int i = 0; i < (subLength - 1); i++) { //napr "ats" 3-1 =2 pocita jen po 't'(aby se neprepsalo posledni s)
        jump[(int) search[i]] = (subLength - 1) - i;
    }

    unsigned int position = subLength - 1; //zaciname od nuly (porovnava se konec substringu)
    int match = 0; //shodnych
    while (position < strlen(string)) {
        while (string[position - match] == search[(subLength - 1) - match]) {

            match++;
            if (match == subLength - 1) { //souhlasi cely search
                return (position - match) + 1; //pocitano od jednicky
            }
        }
        if (match > 0) {
            position += match;
        } else {
            position += jump[(int) string[position]];
        }
        match = 0;
    }

    return 0; //not found
}

char* sort(char* string) {
    int i;
    int sLength = strlen(string);
    if (sLength <= 1) return string;
    int center = sLength / 2;
    char *left = malloc(center);
    if (left == NULL) {
        warning("Chyba funkce realloc - funkce sort - left");
        return NULL;
    }
    char *right = malloc(sLength - center);
    if (right == NULL) {
        warning("Chyba funkce realloc - funkce sort - right");
        return NULL;
    }
    left[center] = '\0';
    right[sLength - center] = '\0';
    for (i = 0; i < center; i++) {
        left[i] = string[i];
    }
    for (i = center; i < sLength; i++) {
        right[i - center] = string[i];
    }
    left = sort(left);
    right = sort(right);
    return merge(left, right);
}

char* merge(char* l, char* r) {
    int i = 0;
    int j = 0;
    int lLength = strlen(l);
    int rLength = strlen(r);
    char* result = malloc(sizeof (char) * (lLength + rLength + 1));
    if (result == NULL) {
        warning("Chyba funkce realloc - funkce merge");
        return NULL;
    }
    result[lLength + rLength] = '\0';
    while (i < lLength && j < rLength) {
        if (l[i] < r[j]) {
            result[i + j] = l[i];
            i++;
        } else {
            result[i + j] = r[j];
            j++;
        }
    }
    if (i < lLength) {
        while (i < lLength) {
            result[i + j] = l[i];
            i++;
        }
    } else {
        while (j < rLength) {
            result[i + j] = r[j];
            j++;
        }
    }
    return result;
}

int addNotFreeTable(htab_t* table) {
    tablesToFree_t* temp;
    tablesToFree_t* move;
    if (table == NULL) {
        warning("tabulka symbolu neni vytvorena\n");
        return ERROR_INTERNAL;
    }
    if ((temp = (tablesToFree_t *) malloc(sizeof (struct tablesFree))) == NULL) { //test jestli se mu podarilo alokovat dostatek mista v pameti
        warning("alokace pameti se nezdarila pro polozku seznamu tabulek k uvolneni");
        return ERROR_INTERNAL;
    }

    temp->table = table;
    temp->next = NULL;
    move = notFreeTables; //globalni tabulka
    if (notFreeTables == NULL) { //prvni prvek
        notFreeTables = temp;
    } else {
        while (move != NULL && move->next != NULL) {
            move = move->next;
        }
        move->next = temp;
    }

    return OK;
}

int freeNotFreeTables(void) {
    tablesToFree_t *toBeDisposed;

    while (notFreeTables != NULL) {
        toBeDisposed = notFreeTables;
        htabFree(notFreeTables->table);
        notFreeTables = notFreeTables->next;
        free(toBeDisposed);
    }
    //notFreeTables = NULL;
    return OK;
}

int addNotFreeItem(listitem_t* item) {
    listOfItemsFree_t* temp;
    listOfItemsFree_t* move;
    if (item == NULL) {
        warning("listitem_t neni vytvoren\n");
        return ERROR_INTERNAL;
    }
    if ((temp = (listOfItemsFree_t *) malloc(sizeof (struct listOfItemsFree))) == NULL) { //test jestli se mu podarilo alokovat dostatek mista v pameti
        warning("alokace pameti se nezdarila pro polozku seznamu itemu k uvolneni");
        return ERROR_INTERNAL;
    }

    temp->item = item;
    temp->next = NULL;
    move = notFreeItems; //globalni seznam
    if (notFreeItems == NULL) { //prvni prvek
        notFreeItems = temp;
    } else {
        while (move != NULL && move->next != NULL) {
            move = move->next;
        }
        move->next = temp;
    }

    return OK;
}

int freeNotFreeItems(void) {
    listOfItemsFree_t *toBeDisposed;
    listitem_t * item;
    listitem_t* tmp;

    while (notFreeItems != NULL) {
        toBeDisposed = notFreeItems;
        item = notFreeItems->item;

        tmp = item;

        if (tmp->key != NULL) {
            free((char*) tmp->key);
            item->key = NULL;
        }
        if (tmp->string != NULL) {
            free((char*) tmp->string);
            item->string = NULL;
        }

        free(tmp);

        notFreeItems = notFreeItems->next;
        free(toBeDisposed);
    }
    return OK;
}

int htabAdd(htab_t* table, const char* key, listitem_t *newItem) {
    if (table == NULL) {
        fprintf(stderr, "%d:%d:Internal:htabLookup:Tabulka neni inicializovana\n", row, character);
        return ERROR_INTERNAL;
    }
    if (strcmp(key, "") == 0) {
        fprintf(stderr, "Zadany retezec je prazdny\n");
        return ERROR_INTERNAL;
    }
    listitem_t *posledni = NULL;
    listitem_t *item;
    int index = hashFunction(key, table->htabSize);
    item = table->listArray[index];

    while (item != NULL) {
        if (strcmp(item->key, key) == 0) { //pokud je key v tabulce
            if (newItem != NULL) {
                warning("%d:%d:Semantic:attempt of redeclaration of id: %s\n", row, character, key);
                return ERROR_SEMANTIC;
            }
        }
        posledni = item;
        item = item->next;
    }

    if (newItem != NULL) {
        if (posledni != NULL) {
            posledni->next = newItem;
        } else {
            table->listArray[index] = newItem;
        }
    }

    return OK;
}

int addPredefFunction(htab_t *table, const char *key, char type, const char argc, ...) {
    int result;
    listitem_t *newItem;
    paramItem_t *params = NULL;
    char* id;
    unsigned char pType;
    va_list args;
    va_start(args, argc);
    for (int i = 1; i < argc; i += 2) {
        id = va_arg(args, char*);
        pType = va_arg(args, int);
        //printf("pridavam param: %s type: %d\n", id, pType);
        paramItemAdd(&params, id, pType);
    }
    va_end(args);
    if ((newItem = malloc(sizeof (listitem_t))) == NULL) {
        warning("internal: memory allocation failed for %s\n", key);
        return ERROR_INTERNAL;
    }
    if ((id = malloc(sizeof (char) *(strlen(key) + 1))) == NULL) {
        warning("internal: memory allocation failed for %s name\n", key);
        return ERROR_INTERNAL;
    }
    strcpy(id, key);
    newItem->key = id;
    newItem->type = type;
    newItem->idType = DEFINED;
    newItem->params = params;
    newItem->string = '\0';
    newItem->value = 0; //inicializace hodnoty, ve fci se ale nepouziva
    newItem->local = NULL;
    newItem->next = NULL;
    newItem->instructions = NULL;
    result = htabAdd(table, key, newItem);
    if (result != OK) {
        paramItem_t *param;
        paramItem_t *tmpParam;
        param = newItem->params;
        while (param->next != NULL) {
            tmpParam = param->next;
            param->next = tmpParam->next;
            free(tmpParam);
        }
        free((char*) newItem->key);
        free((char*) newItem->string);
        free(newItem);
        return result;
    }
    return OK;

}

int declareVar(htab_t* table, const char* key, char type, int initialized) {
    int result;
    listitem_t *newItem;
    if ((newItem = malloc(sizeof (listitem_t))) == NULL) {
        warning("internal: memory allocation failed for %s\n", key);
        return ERROR_INTERNAL;
    }

    if ((newItem->key = malloc(sizeof (char) * (strlen(key) + 1))) == NULL) {
        warning("%d:%d: internal memory allocation for variable %s failed\n", row, character, key);
        return ERROR_INTERNAL;
    };
    strcpy((char *) newItem->key, key);
    newItem->type = type;
    newItem->idType = initialized;
    newItem->value = 0;
    newItem->params = NULL;
    newItem->local = NULL;
    newItem->string = '\0';
    newItem->instructions = NULL;

    newItem->next = NULL;
    result = htabAdd(table, key, newItem);
    if (result != OK) {
        free((char*) newItem->key);
        free((char*) newItem->string);
        free(newItem);
        return result;
    }
    return OK;
}

int assignVar(htab_t* table, const char* key, char type, double value, char* string) {
    listitem_t* newItem;
    newItem = htabLookup(table, key);
    if (type == STRING) {
        free((char*) string);
        char* pole;
        if ((pole = malloc(strlen(string) + 1)) == NULL) {
            warning("internal: memory allocation failed for %s value %s\n", key, string);
            return ERROR_INTERNAL;
        }
        strcpy(pole, string);
        newItem->string = pole;
    } else {
        newItem->value = value;
    }
    return OK;
}

int declareFunc(htab_t* table, const char* key, char type, paramItem_t* params, htab_t** localTable) {
    int result;
    listitem_t *newItem;
    if ((newItem = malloc(sizeof (listitem_t))) == NULL) {
        warning("internal: memory allocation failed for %s\n", key);
        return ERROR_INTERNAL;
    }
    if ((newItem->key = malloc(sizeof (char) * (strlen(key) + 1))) == NULL) {
        warning("%d:%d: internal memory allocation for variable %s failed\n", row, character, key);
        return ERROR_INTERNAL;
    };
    strcpy((char *) newItem->key, key);
    newItem->type = type;
    newItem->idType = DECLARED;
    newItem->string = '\0';
    newItem->value = 0; //inicializace hodnoty, ve fci se ale nepouziva
    if ((newItem->instructions = malloc(sizeof (tListItem))) == NULL) {
        warning("internal: memory allocation failed for %s\n", key);
        return ERROR_INTERNAL;
    }
    newItem->params = params;
    newItem->next = NULL;

    if ((newItem->local = htabInit(LOCAL_TABLE_SIZE)) == NULL) {
        warning("internal: memory allocation failed for %s\n", key);
        return ERROR_INTERNAL;
    }

    if ((result = declareVar(newItem->local, key, type, UNINIT)) != OK) { //promenna pro navratovou hodnotu fce
        return result;
    }
    while (params != NULL) {
        if ((result = declareVar(newItem->local, params->id, params->type, INIT)) != OK) {
            return result;
        }
        params = params->next;
    }

    *localTable = newItem->local;

    result = htabAdd(table, key, newItem);
    if (result != OK) {

        paramItemFree(&(newItem->params));
        htabFree(newItem->local);
        free((char*) newItem->key);
        free((char*) newItem->string);
        free(newItem);


        return result;
    }
    return OK;
}

int defineFunc(htab_t* table, const char* key, char type, paramItem_t* params, htab_t** localTable) {
    listitem_t *item;
    int result;

    item = htabLookup(table, key);
    if (item != NULL) {
        if (item->idType == DECLARED) {
            *localTable = item->local;
            result = OK;
            paramItem_t* paramsDefined = params;
            paramItem_t* paramsDeclared = item->params;
            if (item->type != type) {
                warning("%d:%d:Semantic:Different return type of %s\n", row, character, key);
                return ERROR_SEMANTIC_TYPE;
            }

            while (paramsDefined && paramsDeclared) { //cyklus porovnava shodu parametru definovane a deklarovane funkce
                if ((paramsDefined->type == paramsDeclared->type) && (strcmp(paramsDeclared->id, paramsDefined->id) == 0)) {
                    paramsDefined = paramsDefined->next;
                    paramsDeclared = paramsDeclared->next;
                } else {
                    warning("%d:%d:Semantic:parameters of declaration & definition of %s do not match!\n", row, character, key);
                    return ERROR_SEMANTIC_TYPE;
                }
            }
            if (!(paramsDeclared) && !(paramsDefined)) { //pokud je definovanych i deklarovanych stejny pocet
                result = OK;
                item->idType = DEFINED;
            } else {
                warning("%d:%d:Semantic:parameters of declaration & definition of %s do not match!\n", row, character, key);
                result = ERROR_SEMANTIC_TYPE;
            }

            //            while (params != NULL) {
            //                 paramsDefined = params;
            //                 params = params->next;
            //                 free(paramsDefined);
            //            }
            paramItemFree(&params); //uvolneni po overeni
            return result;

        } else {
            warning("semantic:attempt of redefinition of id: %s\n", key);
            return ERROR_SEMANTIC;
        }
    } else { //neni deklarovana
        if ((item = malloc(sizeof (listitem_t))) == NULL) {
            warning("internal: memory allocation failed for %s\n", key);
            return ERROR_INTERNAL;
        }

        if ((item->key = malloc(sizeof (char) * (strlen(key) + 1))) == NULL) {
            warning("%d:%d: internal memory allocation for variable %s failed\n", row, character, key);
            return ERROR_INTERNAL;
        };
        strcpy((char *) item->key, key);
        item->type = type;
        item->idType = DEFINED;
        item->params = params;
        item->string = '\0';
        item->value = 0; //inicializace hodnoty, ve fci se ale nepouziva
        if ((item->instructions = malloc(sizeof (tListItem))) == NULL) {
            warning("internal: memory allocation failed for %s\n", key);
            return ERROR_INTERNAL;
        }
        if ((item->local = htabInit(LOCAL_TABLE_SIZE)) == NULL) {
            warning("internal: memory allocation failed for %s\n", key);
            return ERROR_INTERNAL;
        }

        if ((result = declareVar(item->local, key, type, UNINIT)) != OK) { //promenna pro navratovou hodnotu fce
            return result;
        }

        while (params != NULL) {
            if ((result = declareVar(item->local, params->id, params->type, INIT)) != OK) {
                return result;
            }
            params = params->next;
        }

        *localTable = item->local;
        item->next = NULL;
        result = htabAdd(table, key, item);
    }

    return result;
}

unsigned char returnType(htab_t* table, htab_t* localTable, const char *key) {
    listitem_t *p = NULL;

    if (localTable != NULL) {
        if ((p = htabLookup(localTable, key)) == NULL) {
            p = htabLookup(table, key);
        }
    } else {
        p = htabLookup(table, key);
    }

    if (p == NULL) {
        warning("%d:%d:Type missmatch in expression, promenna ''%s'' neni v tabulceSymoblu.\n", row, character, key); //takova promenna neni, POZOR NEMENIT ENUMY, 
        return ERROR_SEMANTIC;
    }
    return p->type;
}

int paramItemAdd(paramItem_t **list, char* id, unsigned char type) {
    paramItem_t *head = NULL;
    head = *list;
    paramItem_t *p = NULL;
    if ((p = (paramItem_t *) malloc(sizeof (struct paramItem))) == NULL) {
        warning("%d:%d: Nepodarilo se alokovat misto pro parametr", row, character);
        return ERROR_INTERNAL;
    } //test jestli se mu podarilo alokovat dostatek mista v pameti


    p->id = id;
    p->type = type;
    p->next = NULL;
    p->string = NULL;

    if (*list == NULL) { //pokud jsem prvni prvek v seznamu
        *list = p;
    } else {
        while ((head)->next) {
            (head) = (head)->next;
        }
        (head)->next = p;
    }
    return OK;
}

int paramItemUpdate(paramItem_t **list, char* string, double value, char* id) {
    paramItem_t *last = NULL;
    if ((last = *list) == NULL) {
        warning("%d:%d: update parametru nenalezl seznam.", row, character);
        return ERROR_SEMANTIC_OTHER;
    };

    while (last->next) { //prociklime na konec seznamu, kde je aktualni polozka
        last = last->next;
    }

    last->value = value;
    if (string != NULL) {
        if ((last->string = malloc(sizeof (char) * (strlen(string)) + 1)) == NULL) {
            warning("%d:%d: Nepodarilo se alokovat misto pro retezec v parametru", row, character);
            return ERROR_INTERNAL;
        };
        strcpy(last->string, string);
    }
    if (id != NULL) {
        if ((last->id = malloc(sizeof (char) * (strlen(id)) + 1)) == NULL) {
            warning("%d:%d: Nepodarilo se alokovat misto pro retezec v parametru", row, character);
            return ERROR_INTERNAL;

        }
        strcpy(last->id, id);
    }
    return OK;
}

void paramItemFree(paramItem_t **list) {
    paramItem_t *toBeDisposed;

    while (*list) {
        toBeDisposed = *list;
        *list = (*list)->next;
        if (toBeDisposed->id != NULL) {
            free(toBeDisposed->id);
        }
        if (toBeDisposed->string != NULL) {
            free(toBeDisposed->string);
        }
        free(toBeDisposed);
    }
    *list = NULL;
    return;
}

/* Initialize a tablestack object. */
void tableStackInit(tableStack *s) {
    *s = NULL;
    return;
}

int tableStackPush(tableStack *s, htab_t* table) {
    tableStack_t *p;

    if ((p = malloc(sizeof (struct nodeTableStruct))) == NULL) {
        warning("internal: memory allocation failed for one table in tableStack\n");
        return ERROR_INTERNAL;
    }

    p->table = table;
    if (!*s) {
        p->next = NULL;
    } else {
        p->next = *s;
    }
    *s = p;

    return OK;
}

void tableStackPop(tableStack *s) {
    tableStack_t *p;
    p = *s;
    (*s) = (*s)->next;
    htabFree(p->table);
    free (p);
    return;
}

void tableStackDispose(tableStack *s) {
    while (*s) {
        tableStackPop(s);
    }
    *s = NULL;
    return;
}
