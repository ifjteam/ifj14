#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include "scannersim.h"

int i = 0;

int getTokenSim(token_t *token) {
    char* string1[1000] = {
        "\0",
        "a", "\0", "integer", "\0",
        "b", "\0", "integer", "\0",

        "\0", "factorial", "(", "a", ":", "integer", ";", "h", ":", "real", ")", ":", "integer", ";", "\0", ";",
        "\0", "factorial", "(", "a", ":", "integer", ";", "h", ":", "real", ")", ":", "integer", ";",
        "\0",
            "c", ":", "integer", ";",
        "\0",
            "a", "\0", "42", "\0",
        "\0", "\0",
                
        "\0", "factorial", "(", "a", ":", "integer", ";", "h", ":", "real", ")", ":", "integer", ";",
        "\0",
            "c", ":", "integer", ";",
        "\0",
            "a", "\0", "42", "\0",
        "\0", "\0",

        "\0",
            "a", "\0", "42", "\0",
            "a", "\0", "12", "\0",
            "\0", "a","\0", "42", "\0",  //ifStatement
            "\0",
                "b", "\0", "2", "\0",
                "\0", "b","\0", "2,124", "\0", //whileStatement
                "\0",
                    "a", "\0", "12", "\0",
                "\0",
            "\0",
        "\0", "\0"};

    int test1[1000] = {
        VAR,
        ID, COLON, TINTEGER, SEMICOLON,
        ID, COLON, TINTEGER, SEMICOLON,
        FUNCTION, ID, LBRACKET, ID, COLON, TINTEGER, SEMICOLON, ID, COLON, TREAL, RBRACKET, COLON, TINTEGER, SEMICOLON, FORWARD, SEMICOLON,
        FUNCTION, ID, LBRACKET, ID, COLON, TINTEGER, SEMICOLON, ID, COLON, TREAL, RBRACKET, COLON, TINTEGER, SEMICOLON,
        VAR,
            ID, COLON, TINTEGER, SEMICOLON,
        BEGIN,
            ID, ASSIGNMENT, INTEGER, SEMICOLON,
        END, SEMICOLON,
        
        FUNCTION, ID, LBRACKET, ID, COLON, TINTEGER, SEMICOLON, ID, COLON, TINTEGER, RBRACKET, COLON, TINTEGER, SEMICOLON,
        VAR,
            ID, COLON, TINTEGER, SEMICOLON,
        BEGIN,
            ID, ASSIGNMENT, INTEGER, SEMICOLON,
        END, SEMICOLON,

        BEGIN,
            ID, ASSIGNMENT, INTEGER, SEMICOLON,
            ID, ASSIGNMENT, INTEGER, SEMICOLON,
            IF, ID, EQUAL, INTEGER, THEN,
            BEGIN,
                ID, ASSIGNMENT, ID, SEMICOLON,
                WHILE, ID, LEQUAL, REAL, DO, 
                BEGIN,
                    ID, ASSIGNMENT, INTEGER,
                END,
            END,
        END, DOT};


  char* string5[1000] = {
        "\0",
        "a", "\0", "real", "\0",
        "b", "\0", "integer", "\0",

        "\0", "factorial", "(", "a", ":", "integer", ";", "h", ":", "integer", ")", ":", "integer", ";", "\0", ";",
        "\0", "factorial", "(", "a", ":", "integer", ";", "h", ":", "integer", ")", ":", "integer", ";",
        "\0",
            "c", ":", "integer", ";",
        "\0",
            "a", "\0", "factorial", "\0", "2", "\0", "b", "\0" "\0",
        "\0", "\0",

        "\0",
            "a", "\0", "42", "\0",
            "a", "\0", "12", "\0",
            "\0", "a","\0", "42", "\0",  //ifStatement
            "\0",
                "b", "\0", "2", "\0",
                "\0", "b","\0", "2,124", "\0", //whileStatement
                "\0",
                    "a", "\0", "12", "\0",
                "\0",
            "\0",
        "\0", "\0"};

    int test5[1000] = {
        VAR,
        ID, COLON, TREAL, SEMICOLON,
        ID, COLON, TINTEGER, SEMICOLON,
        FUNCTION, ID, LBRACKET, ID, COLON, TINTEGER, SEMICOLON, ID, COLON, TINTEGER, RBRACKET, COLON, TINTEGER, SEMICOLON, FORWARD, SEMICOLON,
        FUNCTION, ID, LBRACKET, ID, COLON, TINTEGER, SEMICOLON, ID, COLON, TINTEGER, RBRACKET, COLON, TINTEGER, SEMICOLON,
        VAR,
            ID, COLON, TINTEGER, SEMICOLON,
        BEGIN,
            ID, ASSIGNMENT, ID, LBRACKET, INTEGER, COMMA, ID, RBRACKET,SEMICOLON,
        END, SEMICOLON,

        BEGIN,
            ID, ASSIGNMENT, INTEGER, SEMICOLON,
            ID, ASSIGNMENT, INTEGER, SEMICOLON,
            IF, ID, EQUAL, INTEGER, THEN,
            BEGIN,
                ID, ASSIGNMENT, ID, SEMICOLON,
                WHILE, ID, LEQUAL, REAL, DO, 
                BEGIN,
                    ID, ASSIGNMENT, INTEGER,
                END,
            END,
        END, DOT};






    int test2[100] = {
        BEGIN,
        ID, ASSIGNMENT, INTEGER, PLUS, ID, TIMES, REAL, SEMICOLON,
        END, DOT
    };

    int test3[100] = {VAR,
        ID, COLON, TINTEGER, SEMICOLON,
        ID, COLON, TINTEGER, SEMICOLON,
        FUNCTION, ID, LBRACKET, ID, COLON, TINTEGER, RBRACKET, COLON, TINTEGER, SEMICOLON, //chyba RBRACKET
        VAR,
        ID, COLON, TINTEGER, SEMICOLON,
        BEGIN,
        ID, ASSIGNMENT, INTEGER, SEMICOLON,
        END, SEMICOLON,
        BEGIN,
        ID, ASSIGNMENT, INTEGER, SEMICOLON,
        END, DOT};

    //if (i == 2) return 1; //lexical chyba

    token->type = test5[i];
#define SYNTAX_TEST 0 //tady se prepina 0 nebo 1
#if SYNTAX_TEST
    token->string = "\0"; //==1
#else
    token->string = string5[i]; //==0
#endif

    if (0) {
        printf("%i %s %i %i %s %i", test1[0], string1[0], test2[0], test3[0], string5[0], test5[0]); //Unused variables..
    }

    i++;
    return 0;
}
