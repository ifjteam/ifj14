/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */	
/*		Velecký Lukáš	xvelec05      								 */	
/*		Zemek Martin	xzemek04									 */	
/*																	 */
/*********************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "scanner.h"
#include "error.h"
#include "main.h"

FILE *source;
int row = 1; //cislo radku
int character = 0;  //poradi znaku na radku
int tokenChar = 0;
char *tokenContent;
char *tmp;
int stringCompareOutput = 0;

void openFile(FILE *f) //otevriani souboru
{
    source = f;
}

void freeToken(token_t *token) { //uvolneni alokovane pameti
    free(token->string);
    free(tokenContent);
}

int toBase2 (char *n)
{
    int multiplier = 1;
    int result = 0;
    for ( int i = strlen(n)-1; i >= 0; i--)
    {
        result = result + (n[i]-'0')*multiplier;
        multiplier = multiplier * 2;
    }
    return result;
}

int toBase8 (char *n)
{
    int multiplier = 1;
    int result = 0;
    for ( int i = strlen(n)-1; i >= 0; i--)
    {
        result = result + (n[i]-'0')*multiplier;
        multiplier = multiplier * 8;
    }
    return result;
}

int toBase16 (char *n)
{
    int multiplier = 1;
    int result = 0;
    for ( int i = strlen(n)-1; i >= 0; i--)
    {
        if (n[i] >= 'A' && n[i] <= 'F') result = result + (n[i]-'A'+10)*multiplier;
        else if (n[i] >= 'a' && n[i] <= 'f') result = result + (n[i]-'a'+10)*multiplier;
        else result = result + (n[i]-'0')*multiplier;
        multiplier = multiplier * 16;
    }
    return result;
}

int stringCompare(char *s1, char *s2)   //porovnavani dvou stringu (vcetne ignorovani velikosti pismen u klicovych slov) a s kontrolou mallocu
{
    char *internalString = malloc(1);
    if (internalString == NULL) 
    {
        warning("%d:%d Chyba funkce realloc", row, character); 
        return ERROR_INTERNAL;
    }
    int i = 0;
    internalString[i] = '\0';
    for (i = 0; s1[i]; i++)
    {        
        tmp = realloc(internalString, (i+2));
        if (tmp == NULL) 
        { 
            warning("%d:%d Chyba funkce realloc", row, character); 
            return ERROR_INTERNAL; 
        }
        else internalString = tmp;
        
        if (s1[i] >= 'A' && s1[i] <= 'Z') { internalString[i] = s1[i] + 'a' - 'A'; }
        else { internalString[i] = s1[i]; }
        internalString[i+1] = '\0';
    }
    if (strcmp(internalString, s2) == 0) { free(internalString); return 0; }
    else { free(internalString); return 1;  }
}

int getToken(token_t *token) {
    char c, cp, cp2; //aktualni znak, znak o jedno dale, znak o 2 dale
    int state = 0;
    int inString = 0;
    int inComment = 0;
    int inToken = 0;
    tokenChar = 0;
    int realWithE = 0;
    int realWithSign = 0;
    int baseResult;
    int inBase2 = 0;    //dvojkova soustava, zacina znakem %
    int inBase8 = 0;    //osmicková soustava, zacina znakem &
    int inBase16 = 0;   //sestnactková soustava, zacina znakem $
    
    tmp = realloc(tokenContent, (tokenChar+1));
    if (tmp == NULL) { warning("%d:%d Chyba funkce realloc", row, character); return ERROR_INTERNAL; }
    else tokenContent = tmp;
    
    int checkRealloc = 0;
    
    tokenContent[0] = '\0';
    int escapeSequence = 0;
    int escapeContent = 0;
    while ((c = fgetc(source)))
    {
        if (c == '\n')
        {
            if (inString == 1)
            {
                warning("%d:%d Neplatny retezec - radek v retezci\n", row, character);
                return ERROR_LEXICAL;
            }
            row++;
            character=0;
        }
        character++;
        tokenChar++;
        
        if(escapeSequence == 1)
        {
            if (!isdigit((unsigned)c) && c != '\'')
            {
                warning("%d:%d Spatne zadana escape sekvence\n", row, character);
                return ERROR_LEXICAL;
            }
        }
        
        if (c == EOF && inString == 1)
        {
            warning("%d:%d Neukonceny string\n", row, character);
            return ERROR_LEXICAL;
        }              
        
        if (((c == ' ' || c == '\t' || c == '\r' || c == '\n' || c == '\'') && inString != 1) || inComment == 1) 
        { 
            if (tokenChar > 0) tokenChar--; 
        }
        else inToken = 1;            
                
        tmp = realloc(tokenContent, (tokenChar+1));
        if (tmp == NULL) 
        { 
            warning("%d:%d Chyba funkce realloc", row, character); 
            return ERROR_INTERNAL; 
        }
        else tokenContent = tmp;
        
        if (inString == 1 && c <= 31)
        {
            warning("%d:%d Spatne zadany string - obsahuje nepovolene znaky\n", row, character);
            return ERROR_LEXICAL;
        }
       
        tokenContent[tokenChar] = '\0';
        if (inString == 1 && c != '\'' && escapeSequence == 0) tokenContent[tokenChar-1] = c; //znak soucasti stringu       
        else if (inComment == 1 && c != '}' && c != EOF) {}     
        else if ((c == 'e' || c == 'E') && (state == REAL || state == INTEGER))
        {
            cp = fgetc(source);
            if (realWithE == 1)
            {
                warning("%d:%d Spatne zapsany real - 2 E jdouci po sobe \n", row, character);
                return ERROR_LEXICAL;
            }
            else if (!(isdigit((unsigned)cp)) && cp != '+' && cp != '-')
            {
                warning("%d:%d Spatne zapsany real - chybejici exponent \n", row, character);
                return ERROR_LEXICAL;
            }
            ungetc(cp, source);
            realWithE = 1;
            state = REAL;
            tokenContent[tokenChar-1] = c;            
        }
        else if (isalpha((unsigned)c))
        {
            if (state != STRING) state = ID;
            if (state == ID)
            {
                if (c >= 'A' && c <= 'Z') c = c + 'a' - 'A';
            }
            tokenContent[tokenChar-1] = c;
        }
        
        else if (isdigit((unsigned)c))
        {
            if (escapeSequence == 1) // reseni escape sekvence
            {
                escapeContent = escapeContent*10 + (c - '0');      
                if (escapeContent > 255)
                {
                    warning("%d:%d Spatne zadana escape sekvence - mimo interval <1,255> \n", row, character);
                    return ERROR_LEXICAL;
                }
                tokenChar--;
            }
            else if (state != ID && state != STRING)
            {
                if (state != REAL) state = INTEGER;
                tokenContent[tokenChar-1] = c;
            }
            else if (state == ID) tokenContent[tokenChar-1] = c;      
        }
        
        else if (c == '\'')
        {
            cp = fgetc(source);
            if (cp == '#' && inString == 1) //nasleduje excape sekvence
            { 
                escapeSequence = 1; 
                tokenChar--; 
            }
            else if (escapeSequence == 1) //ukonceni escape sekvence
            {
                if (escapeContent <= 0)
                {
                    warning("%d:%d Spatne zadana escape sekvence - mimo interval <1,255> \n", row, character);
                    return ERROR_LEXICAL;
                }
                tokenContent[tokenChar-1] = (char)escapeContent;
                escapeSequence = 0;
                escapeContent = 0;
                ungetc(cp, source);
            }
            else {
                if (inString == 1 && cp == '\'')    //zapsani znaku ' ve stringu
                {
                    tokenContent[tokenChar-1] = c;                    
                }
                else
                {
                    if (inString == 0)  //zmena stavu stringu
                    { 
                        inString = 1; ungetc(cp, source); 
                    }    
                    else    //ukonceni stringu
                    {
                        ungetc(cp, source);
                        tokenContent[tokenChar] = '\0';
                        token->type = state;
                        token->string = tokenContent;
                        state = 0;
                        inString = 0;         
                        break;
                    }
                }
            }            
            state = STRING;
        }
        else if (c == '{') 
        { 
            inComment = 1; 
            inToken = 0; 
            tokenChar--;
        }             
        else if (c == '}') 
        { 
            inComment = 0; 
        }  
        else if (c == '.') 
        {
            cp = fgetc(source);
            if (state != INTEGER)
            {                
                tmp = realloc(tokenContent, (2));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp;

                state = DOT;
                tokenContent[0] = '.';
                tokenContent[1] = '\0';
                token->type = state;
                token->string = tokenContent;
                state = 0;
                inString = 0; 
                ungetc(cp, source);
                break;                
            }
            else if (state == INTEGER)
            {
                if (isdigit((unsigned)cp)) 
                { 
                    ungetc(cp, source); 
                    state = REAL; 
                    tokenContent[tokenChar-1] = c; 
                }
                else
                {
                    ungetc(cp, source);
                    warning("%d:%d Spatne zadany integer nebo real - real bez desetinne casti\n", row, character);
                    return ERROR_LEXICAL;
                }
            }            
        }
        else if (c == '_') 
        { 
            state = ID; 
            tokenContent[tokenChar-1] = c;
        }     
        else if (c == ',') 
        { 
            state = COMMA; 
            
            tmp = realloc(tokenContent, (2));
            if (tmp == NULL) 
            { 
                warning("%d:%d Chyba funkce realloc", row, character); 
                return ERROR_INTERNAL; 
            }
            else tokenContent = tmp;
            token->type = state;
            tokenContent[0] = ',';
            tokenContent[1] = '\0';
            token->string = tokenContent;
            state = 0;
            inString = 0;         
            break;
        }
        else if (c == ';') 
        { 
            state = SEMICOLON; 
            tmp = realloc(tokenContent, (2));
            if (tmp == NULL) 
            { 
                warning("%d:%d Chyba funkce realloc", row, character); 
                return ERROR_INTERNAL; 
            }
            else tokenContent = tmp; 
            token->type = state;
            tokenContent[0] = ';';
            tokenContent[1] = '\0';
            token->string = tokenContent;
            state = 0;
            inString = 0;         
            break;
        }
        else if (c == ':') 
        { 
            state = COLON; 
            cp = fgetc(source);
            if (cp != '=')
            {
                tmp = realloc(tokenContent, (2));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character);
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp; 
                token->type = state;
                tokenContent[0] = ':';
                tokenContent[1] = '\0';
                token->string = tokenContent;
                state = 0;
                inString = 0;  
                ungetc(cp,source);
                break;           
            }
            else ungetc(cp,source);
        }
        else if (c == '<') 
        { 
            state = LESS; 
            cp = fgetc(source);
            if (cp != '>' && cp != '=')
            { 
                tmp = realloc(tokenContent, (2));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp;
                token->type = state;
                tokenContent[0] = '<';
                tokenContent[1] = '\0';
                token->string = tokenContent;
                state = 0;
                inString = 0;    
                ungetc(cp,source);
                break;           
            }
            else ungetc(cp,source);
        }
        else if (c == '>') 
        { 
            if (state == LESS) 
            { 
                state = NOTEQUAL; 
                tmp = realloc(tokenContent, (3));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp; 
                token->type = state;
                tokenContent[0] = '<';
                tokenContent[1] = '>';
                tokenContent[2] = '\0';
                token->string = tokenContent;
                state = 0;
                inString = 0;         
                break;
            }
            else 
            { 
                state = GREATER; 
                cp = fgetc(source);
                if (cp != '=')
                {
                    tmp = realloc(tokenContent, (2));
                    if (tmp == NULL) 
                    { 
                        warning("%d:%d Chyba funkce realloc", row, character); 
                        return ERROR_INTERNAL; 
                    }
                    else tokenContent = tmp;
                    token->type = state;
                    tokenContent[0] = '>';
                    tokenContent[1] = '\0';
                    token->string = tokenContent;
                    state = 0;
                    inString = 0;  
                    ungetc(cp,source);
                    break;           
                }
                else { ungetc(cp,source); }
            }
        }        
        else if (c == '=')
        {
            if (state == COLON) 
            { 
                state = ASSIGNMENT; 
                tmp = realloc(tokenContent, (3));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp;
                token->type = state;
                tokenContent[0] = ':';
                tokenContent[1] = '=';
                tokenContent[2] = '\0';
                token->string = tokenContent;
                state = 0;
                inString = 0;         
                break;
            }
            else if (state == LESS)
            {
                state = LEQUAL; 
                tmp = realloc(tokenContent, (3));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp; 
                token->type = state;
                tokenContent[0] = '<';
                tokenContent[1] = '=';
                tokenContent[2] = '\0';
                token->string = tokenContent;
                state = 0;
                inString = 0;         
                break;
            }
            else if (state == GREATER) 
            { 
                state = GEQUAL; 
                tmp = realloc(tokenContent, (3));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp; 
                token->type = state;
                tokenContent[0] = '>';
                tokenContent[1] = '=';
                tokenContent[2] = '\0';
                token->string = tokenContent;
                state = 0;
                inString = 0;         
                break;
            }
            else 
            { 
                state = EQUAL; 
                tmp = realloc(tokenContent, (2));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp; 
                token->type = state;
                tokenContent[0] = '=';
                tokenContent[1] = '\0';
                token->string = tokenContent;
                state = 0;
                inString = 0;         
                break;
            }            
        }
        else if (c == '+') 
        { 
            if (state == REAL && realWithSign == 0) 
            {
                tokenContent[tokenChar-1] = c; 
                realWithSign = 1;
            }
            else if (realWithSign == 1)
            {
                warning("%d:%d Spatne zadany real - vice znamenek + v exponentu\n", row, character);
                return ERROR_LEXICAL;
            }
            else 
            {
                state = PLUS; 
                tmp = realloc(tokenContent, (2));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp; 
                token->type = state;
                tokenContent[0] = '+';
                tokenContent[1] = '\0';
                token->string = tokenContent;
                state = 0;
                inString = 0;         
                break;
            }
        }
        else if (c == '-') 
        { 
            if (state == REAL && realWithSign == 0) 
            {
                tokenContent[tokenChar-1] = c; 
                realWithSign = 1;
            }
            else if (realWithSign == 1)
            {
                warning("%d:%d Spatne zadany real - vice znamenek - v exponentu\n", row, character);
                return ERROR_LEXICAL;
            }
            else 
            {
                state = MINUS; 
                tmp = realloc(tokenContent, (2));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp; 
                token->type = state;
                tokenContent[0] = '-';
                tokenContent[1] = '\0';
                token->string = tokenContent;
                state = 0;
                inString = 0;         
                break;
            }
        }
        else if (c == '*') 
        { 
            state = TIMES; 
            tmp = realloc(tokenContent, (2));
            if (tmp == NULL) 
            { 
                warning("%d:%d Chyba funkce realloc", row, character); 
                return ERROR_INTERNAL; 
            }
            else tokenContent = tmp;
            token->type = state;
            tokenContent[0] = '*';
            tokenContent[1] = '\0';
            token->string = tokenContent;
            state = 0;
            inString = 0;         
            break;
        }
        else if (c == '/') 
        { 
            state = DIVISION; 
            tmp = realloc(tokenContent, (2));
            if (tmp == NULL) 
            { 
                warning("%d:%d Chyba funkce realloc", row, character); 
                return ERROR_INTERNAL; 
            }
            else tokenContent = tmp;
            token->type = state;
            tokenContent[0] = '/';
            tokenContent[1] = '\0';
            token->string = tokenContent;
            state = 0;
            inString = 0;         
            break;
        }
        else if (c == '(') 
        { 
            state = LBRACKET; 
            tmp = realloc(tokenContent, (2));
            if (tmp == NULL) 
            { 
                warning("%d:%d Chyba funkce realloc", row, character); 
                return ERROR_INTERNAL; 
            }
            else tokenContent = tmp; 
            token->type = state;
            tokenContent[0] = '(';
            tokenContent[1] = '\0';
            token->string = tokenContent;
            state = 0;
            inString = 0;         
            break;
        }
        else if (c == ')') 
        { 
            state = RBRACKET; 
            tmp = realloc(tokenContent, (2));
            if (tmp == NULL) 
            { 
                warning("%d:%d Chyba funkce realloc", row, character); 
                return ERROR_INTERNAL; 
            }
            else tokenContent = tmp;
            token->type = state;
            tokenContent[0] = ')';
            tokenContent[1] = '\0';
            token->string = tokenContent;
            state = 0;
            inString = 0;         
            break;
        }
        else if(c == '%')
        {
            cp = fgetc(source);
            if(!(cp == '0' || cp == '1'))
            {
                warning("%d:%d Spatne zadane cislo ve dvojkove soustave\n", row, character);
                return ERROR_LEXICAL;
            }
            ungetc(cp, source);
            tokenChar--;
            inBase2 = 1;
            state = INTEGER;
        }        
        
        else if(c == '&')
        {
            cp = fgetc(source);
            if(!(cp == '0' || cp == '1' || cp == '2' || cp == '3' || cp == '4' || cp == '5' || cp == '6' || cp == '7'))
            {
                warning("%d:%d Spatne zadane cislo v osmickove soustave\n", row, character);
                return ERROR_LEXICAL;
            }
            ungetc(cp, source);
            tokenChar--;
            inBase8 = 1;
            state = INTEGER;
        }
        
        else if(c == '$')
        {
            cp = fgetc(source);
            if (!(cp == '0' || cp == '1' || cp == '2' || cp == '3' || cp == '4' || cp == '5' || cp == '6' || cp == '7' || cp == '8' || cp == '9'
                || cp == 'a' || cp == 'A' || cp == 'b' || cp == 'B' || cp == 'c' || cp == 'C' || cp == 'd' || cp == 'D' || cp == 'e' || cp == 'E' || cp == 'f' || cp == 'F'
                ))
            {
                warning("%d:%d Spatne zadane cislo v osmickove soustave\n", row, character);
                return ERROR_LEXICAL;
            }
            ungetc(cp, source);
            tokenChar--;
            inBase16 = 1;
            state = INTEGER;
        }
        
        else if (c == EOF) 
        { 
            tmp = realloc(tokenContent, (1));
            if (tmp == NULL) 
            { 
                warning("%d:%d Chyba funkce realloc", row, character); 
                return ERROR_INTERNAL; 
            }
            else tokenContent = tmp; 
            tokenContent[0] = '\0';
            state = FILE_END; 
            token->type = state;
            token->string = tokenContent;
            state = 0;
            inString = 0;         
            break;
        }
        
        else if (((c == ' ' && inToken == 1 && inString == 0) || (c == '\n' && (inToken == 1 || inComment == 1)) || (c =='\t' && inToken == 1) || c == EOF) && inString == 0) //znak mezery, pokud neni soucasti stringu -> dalsi token
        {            
            if ((stringCompareOutput = stringCompare(tokenContent, "integer")) == 0) { state = TINTEGER; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "real")) == 0) { state = TREAL; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "boolean")) == 0) { state = TBOOLEAN; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "string")) == 0) { state = TSTRING; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "begin")) == 0) { state = BEGIN; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "end")) == 0) { state = END; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "function")) == 0) { state = FUNCTION; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "forward")) == 0) { state = FORWARD; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "if")) == 0) { state = IF; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "false")) == 0) { state = BOOLEAN; tokenChar = 1; tmp = realloc(tokenContent, (tokenChar+1)); tokenContent[0] = '0'; checkRealloc = 1; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "true")) == 0) { state = BOOLEAN; tokenChar = 1; tmp = realloc(tokenContent, (tokenChar+1)); tokenContent[0] = '1'; checkRealloc = 1; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "then")) == 0) { state = THEN; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "else")) == 0) { state = ELSE; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "while")) == 0) { state = WHILE; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "do")) == 0) { state = DO; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "var")) == 0) { state = VAR; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "write")) == 0) { state = WRITE; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "readln")) == 0) { state = READLN; }
            
            if (stringCompareOutput == ERROR_INTERNAL) return ERROR_INTERNAL;
            
            if (checkRealloc == 1)
            {
                tmp = realloc(tokenContent, (tokenChar+1));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    checkRealloc = 0; 
                    return ERROR_INTERNAL;
                }
                else {tokenContent = tmp; checkRealloc = 0;}
            }
            if (inBase2 == 1 || inBase8 == 1 || inBase16 == 1)
            {
                if (inBase2 == 1) baseResult = toBase2(tokenContent);
                else if (inBase8 == 1) baseResult = toBase8(tokenContent);
                else if (inBase16 == 1) baseResult = toBase16(tokenContent);
                int baseResultHelp = baseResult;
                int baseResultLength = 0;
                
                while (baseResultHelp > 0)
                {
                    baseResultLength++;
                    baseResultHelp = baseResultHelp / 10;
                }
                tokenChar = baseResultLength;
                
                tmp = realloc(tokenContent, (baseResultLength));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp;
                for (int i = tokenChar-1; i >= 0 ; i--)
                {
                    tokenContent[i] = (baseResult % 10)+ '0';
                }
                state = INTEGER;
            }
            
            tokenContent[tokenChar] = '\0';
            token->type = state;
            token->string = tokenContent;
            state = 0;
            inString = 0;         
            break;
        }
        else if (inString == 0 && inComment == 0 && c != '\r' && c != '\n' && c!= '\t' && c != ' ' && c != '#') {
           warning("%d:%d Vseobecne neplatny znak\n", row, character);
           return ERROR_LEXICAL; 
        }
        
        //reseni nasledujiciho znaku
        cp = fgetc(source);
        if (((cp == EOF && inToken == 1) || ((cp == 'e' || cp == 'E') && realWithE == 1) ||
                ((cp == ';' || cp == ':' || (cp == '.' && state != INTEGER) || (cp == '=' && (state != COLON && state != GREATER && state != LESS)) 
                || cp == '(' || cp == ')' || cp == '<' || (cp == '>' && state != LESS) || cp == '{'
                || (cp == '+' && realWithE == 0) || (cp == '-' && realWithE == 0) || cp == '*' || cp == '/') && cp != ' ') || (cp == '\r' && inToken == 1) || cp == ',' || cp == '\''
           ) && inString == 0 && inComment == 0 && inToken == 1)
        {
            if ((stringCompareOutput = stringCompare(tokenContent, "integer")) == 0) { state = TINTEGER; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "real")) == 0) { state = TREAL; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "boolean")) == 0) { state = TBOOLEAN; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "string")) == 0) { state = TSTRING; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "begin")) == 0) { state = BEGIN; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "end")) == 0) { state = END; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "function")) == 0) { state = FUNCTION; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "forward")) == 0) { state = FORWARD; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "if")) == 0) { state = IF; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "false")) == 0) { state = BOOLEAN; tokenChar = 1; tmp = realloc(tokenContent, (tokenChar+1)); tokenContent[0] = '0'; checkRealloc = 1; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "true")) == 0) { state = BOOLEAN; tokenChar = 1; tmp = realloc(tokenContent, (tokenChar+1)); tokenContent[0] = '1'; checkRealloc = 1; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "then")) == 0) { state = THEN; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "else")) == 0) { state = ELSE; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "while")) == 0) { state = WHILE; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "do")) == 0) { state = DO; }          
            else if ((stringCompareOutput = stringCompare(tokenContent, "var")) == 0) { state = VAR; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "write")) == 0) { state = WRITE; }
            else if ((stringCompareOutput = stringCompare(tokenContent, "readln")) == 0) { state = READLN; }
            
            if (stringCompareOutput == ERROR_INTERNAL) return ERROR_INTERNAL;
            
            if (checkRealloc == 1)
            {
                if (tmp == NULL) { warning("%d:%d Chyba funkce realloc", row, character); checkRealloc = 0; return ERROR_INTERNAL;}
                else {tokenContent = tmp; checkRealloc = 0;}
            }
            
            if (inBase2 == 1 || inBase8 == 1 || inBase16 == 1)
            {
                if (inBase2 == 1) baseResult = toBase2(tokenContent);
                else if (inBase8 == 1) baseResult = toBase8(tokenContent);
                else if (inBase16 == 1) baseResult = toBase16(tokenContent);
                int baseResultHelp = baseResult;
                int baseResultLength = 0;
                
                while (baseResultHelp > 0)
                {
                    baseResultLength++;
                    baseResultHelp = baseResultHelp / 10;
                }
                tokenChar = baseResultLength;
                
                tmp = realloc(tokenContent, (baseResultLength));
                if (tmp == NULL) 
                { 
                    warning("%d:%d Chyba funkce realloc", row, character); 
                    return ERROR_INTERNAL; 
                }
                else tokenContent = tmp;
                for (int i = tokenChar-1; i >= 0 ; i--)
                {
                    tokenContent[i] = (baseResult % 10) + '0';
                    baseResult = baseResult / 10;
                }
                state = INTEGER;
            }
            
            tokenContent[tokenChar] = '\0';
            token->type = state;
            token->string = tokenContent;
            state = 0;
            inString = 0;
            ungetc(cp, source);
            break;            
        }
        else if (state == INTEGER && (!isdigit((unsigned)cp) && cp != '.') && inBase2 == 0 && inBase8 == 0 && inBase16 == 0)
        {
            cp2 = fgetc(source);
            if (!((cp == 'E' || cp == 'e') && (isdigit((unsigned)cp2) || cp2 == '.' || cp2 == '+' || cp2 == '-' || cp2 == 'e' || cp2 == 'E')))
            {
                ungetc(cp2, source);
                ungetc(cp, source);
                tokenContent[tokenChar] = '\0';
                token->type = state;
                token->string = tokenContent;
                state = 0;
                inString = 0;         
                break;
            }
            else
            {
                ungetc(cp2, source);
                ungetc(cp, source);
            }
        }
        
        else if (inBase2 == 1 && !(cp == '0' || cp == '1'))
        {
            baseResult = toBase2(tokenContent);
            int baseResultHelp = baseResult;
            int baseResultLength = 0;

            while (baseResultHelp > 0)
            {
                baseResultLength++;
                baseResultHelp = baseResultHelp / 10;
            }
            tokenChar = baseResultLength;

            tmp = realloc(tokenContent, (baseResultLength));
            if (tmp == NULL) 
            { 
                warning("%d:%d Chyba funkce realloc", row, character); 
                return ERROR_INTERNAL; 
            }
            else tokenContent = tmp;
            for (int i = tokenChar-1; i >= 0 ; i--)
            {
                tokenContent[i] = (baseResult % 10) + '0';
                baseResult = baseResult / 10;
            }
            state = INTEGER;
            tokenContent[tokenChar] = '\0';
            token->type = state;
            token->string = tokenContent;
            state = 0;
            inString = 0;
            ungetc(cp, source);
            break;
        }
        
        else if (inBase8 == 1 && !(cp == '0' || cp == '1' || cp == '2' || cp == '3' || cp == '4' || cp == '5' || cp == '6' || cp == '7'))
        {
            baseResult = toBase8(tokenContent);
            int baseResultHelp = baseResult;
            int baseResultLength = 0;

            while (baseResultHelp > 0)
            {
                baseResultLength++;
                baseResultHelp = baseResultHelp / 10;
            }
            tokenChar = baseResultLength;

            tmp = realloc(tokenContent, (baseResultLength));
            if (tmp == NULL) 
            { 
                warning("%d:%d Chyba funkce realloc", row, character); 
                return ERROR_INTERNAL; 
            }
            else tokenContent = tmp;
            for (int i = tokenChar-1; i >= 0 ; i--)
            {
                tokenContent[i] = (baseResult % 10) + '0';
                baseResult = baseResult / 10;
            }
            state = INTEGER;
            tokenContent[tokenChar] = '\0';
            token->type = state;
            token->string = tokenContent;
            state = 0;
            inString = 0;
            ungetc(cp, source);
            break;
        }
        else if (inBase16 == 1 && !(cp == '0' || cp == '1' || cp == '2' || cp == '3' || cp == '4' || cp == '5' || cp == '6' || cp == '7' || cp == '8' || cp == '9'
                 || cp == 'a' || cp == 'A' || cp == 'b' || cp == 'B' || cp == 'c' || cp == 'C' || cp == 'd' || cp == 'D' || cp == 'e' || cp == 'E' || cp == 'f' || cp == 'F'
                ))
        {
            baseResult = toBase16(tokenContent);
            int baseResultHelp = baseResult;
            int baseResultLength = 0;

            while (baseResultHelp > 0)
            {
                baseResultLength++;
                baseResultHelp = baseResultHelp / 10;
            }
            tokenChar = baseResultLength;

            tmp = realloc(tokenContent, (baseResultLength));
            if (tmp == NULL) 
            { 
                warning("%d:%d Chyba funkce realloc", row, character); 
                return ERROR_INTERNAL; 
            }
            else tokenContent = tmp;
            for (int i = tokenChar-1; i >= 0 ; i--)
            {
                tokenContent[i] = (baseResult % 10) + '0';
                baseResult = baseResult / 10;
            }
            state = INTEGER;
            tokenContent[tokenChar] = '\0';
            token->type = state;
            token->string = tokenContent;
            state = 0;
            inString = 0;
            ungetc(cp, source);
            break;
        }
        
        else {
            ungetc(cp, source);
        }        
    }
    return OK;
}

