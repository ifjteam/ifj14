/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */
/*		Velecký Lukáš	xvelec05      								 */
/*		Zemek Martin	xzemek04									 */
/*																	 */
/*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ial.h"
#include "parser.h"
//#include "scannersim.h"
#include "scanner.h"
#include "error.h"
#include "psa.h"
#include "generator.h"

token_t token;

paramItem_t *listOfParams;

htab_t* localTable = NULL;

int parse(void) {
    //if (getTokenSim(&token) != 0) return ERROR_LEXICAL;
    getTokenMacro(token)
    return program();
}

int program(void) {
    int result;
    if ((result = declarations()) != OK) return result;

    if (generateInstruction(I_START, NULL, NULL, NULL) == NULL) {
        return ERROR_INTERNAL;
    } //instrukce znaci zacatek programu

    if ((result = coumpndStat()) != OK) return result;
    cmpTokenTypeMacro(token, DOT)

    if (generateInstruction(I_END, NULL, NULL, NULL) == NULL) {
        return ERROR_INTERNAL;
    } //instrukce znaci konec programu
    
    getTokenMacro(token)
    cmpTokenTypeMacro(token, FILE_END)
    
    //kontrola deklarovanych a nedefinovanych funkci
    listitem_t *item;
    for (int j = 0; j < symbolTable->htabSize; j++) {
        item = symbolTable->listArray[j];
        while (item != NULL) {
            if (item->idType == DECLARED) {
                warning("%d:%d:Semantic:Definition for declared function %s not found\n", row, character, item->key);
                return ERROR_SEMANTIC;
            }
            item = item->next;
        }

    }
    return OK;
}

int declarations(void) {
    int result;
    if (token.type == VAR) {
        getTokenMacro(token)
        if ((result = varDec()) != OK) return result;

        if ((result = declrList()) != OK) return result;
    }

    if (token.type == FUNCTION) {
        //nevolame token, FUNCTION se pak overuje znova v rekurzi
        if ((result = funcList()) != OK) return result;

    }

    //neziskava dalsi token, protoze declarations() je dobrovolna
    return OK;
}

int varDec(void) {
    //cmpTokenTypeMacro(token,ID)
    char* id;
    int type;
    int result;

    cmpTokenTypeMacro(token, ID)
    if ((id = malloc(sizeof (char) *(strlen(token.string) + 1))) == NULL) {
        warning("internal: memory allocation failed for %s name\n", token.string);
        return ERROR_INTERNAL;
    }
    strcpy(id, token.string);
    getTokenMacro(token)
    cmpTokenTypeMacro(token, COLON)

    getTokenMacro(token)
    if (token .type != TBOOLEAN && token .type != TINTEGER && token .type != TREAL && token .type != TSTRING) {
        warning("syntax: line:%d: in function:%s: type:%d: expected:TYPE - (22-25)\n", __LINE__, __func__, token .type);
        return ERROR_SYNTAX;
    }
    type = token.type;

    getTokenMacro(token)
    cmpTokenTypeMacro(token, SEMICOLON)

    if (localTable != NULL) {
        result = declareVar(localTable, id, type, UNINIT);
    } else {
        result = declareVar(symbolTable, id, type, UNINIT);
    }

    if (result != OK) return result;
    getTokenMacro(token)
    free(id);
    return OK;
}

int declrList(void) {
    int result;
    if (token.type == ID) {
        if ((result = varDec()) != OK) return result;
        if ((result = declrList()) != OK) return result;
    }

    return OK;
}

int funcList(void) {
    int result;
    char* id;

    if (token.type == FUNCTION) {
        getTokenMacro(token)
        id = malloc(sizeof (char)*(strlen(token.string) + 1)); //alokujeme polozku ID a nahrajeme tam aktualni jmeno fce, ID potrebujeme, aby se dale mohl
        strcpy(id, token.string); //updatovat jeji ukazatel na zacetek jejich instrukci v tabulce symbolu

        if ((result = funcHeader()) != OK) return result;

        if (token.type == FORWARD) {
            //dopredna deklarace
            getTokenMacro(token)
            cmpTokenTypeMacro(token, SEMICOLON)

            getTokenMacro(token)
            if ((result = funcList()) != OK) return result;

            free(id);
            return OK;
            //po forwardu funkce nepokracuje
        }

        if (token.type == VAR) {
            getTokenMacro(token)
            if ((result = declrList()) != OK) return result;
        }

        tListItem* functionBegin = generateInstruction(I_LABEL, NULL, NULL, NULL);
        if (functionBegin == NULL) {
            return ERROR_INTERNAL;
        }

        if (updateFuncInstruction(id, functionBegin) == ERROR_SEMANTIC) {
            return ERROR_SEMANTIC;
        }

        if ((result = coumpndStat()) != OK) return result;

        listitem_t* functionVarLocalTable = htabLookup(localTable, id);
        if (functionVarLocalTable == NULL) {
            return ERROR_SEMANTIC;
        }
        listitem_t* functionListitem_tSymbolTable = htabLookup(symbolTable, id);
        if (functionListitem_tSymbolTable == NULL) {
            return ERROR_SEMANTIC;
        }

        if (generateInstruction(I_FCE_END, functionVarLocalTable, NULL, functionListitem_tSymbolTable) == NULL) {
            return ERROR_INTERNAL;
        }
        cmpTokenTypeMacro(token, SEMICOLON)
        localTable = NULL; //znulujeme odkaz na lokalniTabulku jak vychazime z funkce
        getTokenMacro(token)

        free(id);
        if ((result = funcList()) != OK) return result;
    }
    return OK;
}

int funcHeader(void) {
    int result;

    char* id;
    int type;

    cmpTokenTypeMacro(token, ID)
    if ((id = malloc(sizeof (char) *(strlen(token.string) + 1))) == NULL) {
        warning("internal: memory allocation failed for %s name\n", token.string);
        return ERROR_INTERNAL;
    }
    strcpy(id, token.string);

    getTokenMacro(token)
    if ((result = arguments()) != OK) return result;

    cmpTokenTypeMacro(token, COLON)

    getTokenMacro(token)
    if (token .type != TBOOLEAN && token .type != TINTEGER && token .type != TREAL && token .type != TSTRING) {
        warning("syntax: line:%d: in function:%s: type:%d: expected:TYPE - (23-26)\n", __LINE__, __func__, token .type);
        return ERROR_SYNTAX;
    }
    type = token.type;

    getTokenMacro(token)
    cmpTokenTypeMacro(token, SEMICOLON)

    getTokenMacro(token)
    if (token.type == FORWARD) {
        result = declareFunc(symbolTable, id, type, listOfParams, &localTable);
        if (result != OK) return result;
    } else {
        result = defineFunc(symbolTable, id, type, listOfParams, &localTable);
        if (result != OK) return result;
    }

    free(id);
    return OK;
}

int arguments(void) {
    int result;
    listOfParams = NULL;
    if (token.type == LBRACKET) {
        getTokenMacro(token)

        if ((result = argumentList()) != OK) return result;

        cmpTokenTypeMacro(token, RBRACKET)
        getTokenMacro(token)
    }
    return OK;
}

int argumentList(void) {
    int result;

    if (token.type == RBRACKET) { //seznam parametru muze byt i prazdny
        return OK;
    }
    if ((result = arg()) != OK) return result;

    if (token.type == SEMICOLON) {
        getTokenMacro(token)
        if ((result = argumentList()) != OK) return result;

    }


    return OK;
}

int arg(void) {
    int result;

    cmpTokenTypeMacro(token, ID)

    char* paramId;
    if ((paramId = malloc(sizeof (char) *(strlen(token.string) + 1))) == NULL) {
        warning("internal: memory allocation failed for %s name\n", token.string);
        return ERROR_INTERNAL;
    }
    strcpy(paramId, token.string);

    getTokenMacro(token)
    cmpTokenTypeMacro(token, COLON)

    getTokenMacro(token)
    if (token .type != TBOOLEAN && token .type != TINTEGER && token .type != TREAL && token .type != TSTRING) {
        warning("%d:%d:syntax: line:%d: in function:%s: type:%d: expected:TYPE - (22-25)\n", row, character, __LINE__, __func__, token .type);
        return ERROR_SYNTAX;
    }
    int paramType = token.type;

    result = paramItemAdd(&listOfParams, paramId, paramType);
    if (result != OK) return result;
    getTokenMacro(token)
    return OK;
}

int coumpndStat(void) {
    int result;
    cmpTokenTypeMacro(token, BEGIN)

    getTokenMacro(token)
    if ((result = statList()) != OK) return result;

    cmpTokenTypeMacro(token, END)
    getTokenMacro(token)
    return OK;
}

int statList(void) {
    int result;
    
    if ((result = stat()) != OK) return result;

    if (token.type == SEMICOLON) {
        getTokenMacro(token)
        if (token.type == END) {
            warning("%d:%d: Syntax: Semicolon before end\n", row, character);
            return ERROR_SYNTAX;
        }
        if ((result = statList()) != OK) return result;

    }
    return OK;
}

int stat(void) {
    int result;

    switch (token.type) {
        case BEGIN:
        {
            if ((result = coumpndStat()) != OK) return result;
            break;
        }
        case WRITE:
        {
            getTokenMacro(token)
            cmpTokenTypeMacro(token, LBRACKET)

            getTokenMacro(token)
            while (token.type != RBRACKET && token.type != FILE_END) {
                if (token.type == STRING) {
                    /*printf("%s", token.string); //tady bude instrukce pro tisknuti STRING*/
                    listitem_t* print;
                    if ((print = tokenToListitem_t()) == NULL) {
                        return ERROR_INTERNAL;
                    }
                    if (generateInstruction(I_WRITE, print, NULL, NULL) == NULL) {
                        return ERROR_INTERNAL;
                    }
                } else if (token.type == ID) {
                    listitem_t *promenna;
                    if (localTable != NULL) {
                        promenna = htabLookup(localTable, token.string);
                        if (promenna == NULL) {
                            promenna = htabLookup(symbolTable, token.string);
                        }
                    } else {
                        promenna = htabLookup(symbolTable, token.string);
                    }
                    if (promenna->idType >= DECLARED) { //je to funkce
                        warning("%d:%d:Semantic:Function call not allowed here (%s)", row, character, promenna->key);
                        return ERROR_SEMANTIC_OTHER;
                    }

                    if (promenna->type == TINTEGER || promenna->type == TREAL) {
                        /*printf("%g", funkce->value); //tady bude instrukce pro tisknuti NUMBER*/
                        if (generateInstruction(I_WRITE, promenna, NULL, NULL) == NULL) {
                            return ERROR_INTERNAL;
                        }
                    } else if (promenna->type == TSTRING) {
                        /*printf("%s", funkce->string); //tady bude instrukce pro tisknuti STRING*/
                        if (generateInstruction(I_WRITE, promenna, NULL, NULL) == NULL) {
                            return ERROR_INTERNAL;
                        }
                    }
                } else if (token.type == INTEGER || token.type == REAL) {
                    /*printf("%g", atof(token.string)); //tady bude instrukce pro tisknuti NUMBER*/
                    listitem_t* print;
                    if ((print = tokenToListitem_t()) == NULL) {
                        return ERROR_INTERNAL;
                    }
                    if (generateInstruction(I_WRITE, print, NULL, NULL) == NULL) {
                        return ERROR_INTERNAL;
                    }
                } else if (token.type == STRING) {
                    /*char *string;
                    if ((string = malloc(sizeof (char) *(strlen(token.string) + 1))) == NULL) {         postrebuju string naparsovat to listitem_t, pro interpret, nestaci v promenne string
                        warning("internal: memory allocation failed for %s name\n", token.string);
                        return ERROR_INTERNAL;
                    }
                    strcpy(string, token.string);*/
                    listitem_t* print;
                    if ((print = tokenToListitem_t()) == NULL) {
                        return ERROR_INTERNAL;
                    }
                    if (generateInstruction(I_WRITE, print, NULL, NULL) == NULL) {
                        return ERROR_INTERNAL;
                    }
                    /*printf("%s", string); //tady bude instrukce pro tisknuti STRING*/
                } else if (token.type == BOOLEAN) {
                    listitem_t* print;
                    if ((print = tokenToListitem_t()) == NULL) {
                        return ERROR_INTERNAL;
                    }
                    if (generateInstruction(I_WRITE, print, NULL, NULL) == NULL) {
                        return ERROR_INTERNAL;
                    }
                } else {
                    warning("%d:%d:Neplatny parametr funkce write.\n", row, character);
                    return ERROR_SYNTAX;
                }

                getTokenMacro(token)
                if (token.type != COMMA) {
                    break;
                }
                getTokenMacro(token)
            }
            cmpTokenTypeMacro(token, RBRACKET)
            getTokenMacro(token)
            break;
        }
        case READLN:
        {
            getTokenMacro(token)
            cmpTokenTypeMacro(token, LBRACKET)

            getTokenMacro(token)
            while (token.type != RBRACKET && token.type != FILE_END) {
                if (token.type == ID) {
                    listitem_t *promenna;
                    if (localTable != NULL) {
                        promenna = htabLookup(localTable, token.string);
                        if (promenna == NULL) {
                            promenna = htabLookup(symbolTable, token.string);
                        }
                    } else {
                        promenna = htabLookup(symbolTable, token.string);
                    }
                    if (promenna == NULL) {
                        warning("%d:%d:semantic:undeclared variable %s\n", row, character, token.string);
                        return ERROR_SEMANTIC;
                    }
                    if (promenna->idType <= INIT) { //je to promenna
                        if (promenna->type == TINTEGER || promenna->type == TREAL) {
                            /*scanf("%g", &(promenna->value)); //tady bude instrukce pro tisknuti NUMBER*/
                            if (generateInstruction(I_READ, NULL, NULL, promenna) == NULL) {
                                return ERROR_INTERNAL;
                            }

                        } else if (promenna->type == TSTRING) {
                            /*scanf("%s", promenna->string); //tady bude instrukce pro tisknuti STRING*/
                            if (generateInstruction(I_READ, NULL, NULL, promenna) == NULL) {
                                return ERROR_INTERNAL;
                            }
                        } else if (promenna->type == TBOOLEAN) {
                            warning("%d:%d:Semantic:Boolean ID (%s) not allowed here\n", row, character, promenna->key);
                            return ERROR_SEMANTIC_TYPE;
                        }
                    } else {
                        warning("%d:%d:Semantic:Function call not allowed here (%s)", row, character, promenna->key);
                    }
                }
                getTokenMacro(token)
            }
            cmpTokenTypeMacro(token, RBRACKET)
            getTokenMacro(token)

            break;
        }

        case ID:
        {
            if ((result = assignStat()) != OK) return result;
            break;
        }
        case IF:
        {
            if ((result = ifStat()) != OK) return result;
            break;
        }
        case WHILE:
        {
            if ((result = whileStat()) != OK) return result;
            break;
        }

    }

    return OK;
}

int expression(int type) {
    int a;
    int usedRule;
    int* rRule;
    if ((rRule = malloc(3 * sizeof (int))) == NULL) return ERROR_INTERNAL;
    int tmp;
    int b;
    int expressionType;
    int leftSide = 1;
    int expressionTypeLeftside;
    int expressionTypeRightside;
    int firstOnTheRight = 0;
    listitem_t* listItem;

    if ((token.type == ID) || (token.type == BOOLEAN) || (token.type == INTEGER) || (token.type == REAL) || (token.type == STRING) ||
            (token.type == TBOOLEAN) || (token.type == TINTEGER) || (token.type == TREAL) || (token.type == TSTRING) || (token.type == LBRACKET) || (token.type == RBRACKET)) { //uvodni prirazeni pocatecniho typu, tzn typu prvniho terminalu ve vyrazu
        if (token.type == ID) {
            expressionType = returnType(symbolTable, localTable, token.string);
            if (expressionType == ERROR_SEMANTIC) { //kontrola typu pro promene, fce returnType vraci error pokud promenna neni definovana nebo inicializovana
                return ERROR_SEMANTIC; //potencialni konflikt ERROR_SEMANTIC a nejakeho typu.
            }
        } else {
            expressionType = token.type;
        }
    }
    int i = 0;
    intStack stack;
    intStackInit(&stack);
    //getTokenMacro(token) funkce co vola expression ziska novy token
    if (intStackPush(&stack, DOLLAR) == -1) return ERROR_INTERNAL;


    do {
        a = intStackFirstTerminal(&stack); //PORESIT PRAZDY ZASOBNIK

        rRule[0] = -1; //default state of rule
        rRule[1] = -1;
        rRule[2] = -1;

        if ((token.type == ID) || (token.type == BOOLEAN) || (token.type == INTEGER) || (token.type == REAL) || (token.type == STRING) ||
                (token.type == TBOOLEAN) || (token.type == TINTEGER) || (token.type == TREAL) || (token.type == TSTRING) || (token.type == LBRACKET) || (token.type == RBRACKET)) {
            if (token.type == LBRACKET) {
                b = LBRACKET;
            } else if (token.type == RBRACKET) {
                b = RBRACKET;
            } else {
                b = I;
            }
            if (token.type == ID) {
                tmp = returnType(symbolTable, localTable, token.string);
            } else {
                tmp = token.type;
            }
            //vycet vsech moznych semanticky spravnych porovnani
            if ((expressionType = checkAritmethicSemantics(expressionType, tmp)) == ERROR_SEMANTIC_TYPE) {
                warning("%d:%d:Semanticka chyba, soucasny typ vyrazu je: %d, nelze zpracovat typ: %d\n", row, character, expressionType, tmp);
                free(rRule);
                intStackDispose(&stack);
                return ERROR_SEMANTIC_TYPE;
            }
            if ((tmp != RBRACKET) && (tmp != LBRACKET)) { //pro zavorky negeneruju instrukce push na zasobnik, ani je nemusim zachovavat v listItem
                if ((listItem = tokenToListitem_t()) == NULL) {
                    return ERROR_INTERNAL;
                }
                if (generateInstruction(I_PUSH, listItem, NULL, NULL) == NULL) {
                    return ERROR_INTERNAL;
                }
            }

        } else if ((token.type == THEN) || (token.type == SEMICOLON) || (token.type == END) || (token.type == DO)) {
            b = DOLLAR;
        } else {
            b = token.type;
        }

        switch (getPrecTab(a, b)) {
            case NEXT:
            {
                if ((token.type == ID) || (token.type == BOOLEAN) || (token.type == INTEGER) || (token.type == REAL) || (token.type == STRING) ||
                        (token.type == TBOOLEAN) || (token.type == TINTEGER) || (token.type == TREAL) || (token.type == TSTRING) || (token.type == LBRACKET) || (token.type == RBRACKET)) {
                    if (token.type == LBRACKET) {
                        if (intStackPush(&stack, LBRACKET) == -1) return ERROR_INTERNAL;
                    } else if (token.type == RBRACKET) {
                        if (intStackPush(&stack, RBRACKET) == -1) return ERROR_INTERNAL;
                    } else {
                        if (intStackPush(&stack, I) == -1) return ERROR_INTERNAL;
                    }
                }
                getTokenMacro(token)
                break;
            }
            case SHIFT:
            {
                if (intStackPushBeforeFirstTerminal(&stack, SHIFT) == -1) {
                    warning("%d:%d:Expression syntax error\n", row, character);
                    free(rRule);
                    intStackDispose(&stack);
                    return ERROR_SYNTAX;
                }
                if (intStackPush(&stack, b) == -1) return ERROR_INTERNAL;
                if (b <= LEQUAL) { //kontroluje na ktere strane porovnavaciho vyrazu jsme
                    leftSide = 0;
                    firstOnTheRight = 1;
                }
                getTokenMacro(token)

                if (firstOnTheRight == 1) { //pokud se preslo z leve strany, vyresetujeme hodnotu expessionType, muzeme protoze leva strana je ulozena v expressionTypeLeftside
                    if (token.type == ID) {
                        expressionType = returnType(symbolTable, localTable, token.string);
                    } else {
                        expressionType = token.type;
                    }
                    firstOnTheRight = 0;
                }

                break;
            }
            case REDUCT:
            {
                i = 0;
                while ((tmp = intStackPop(&stack)) != SHIFT) { //beru ze zasobniku tak dlouho dokud nenarazim na SHIFT, nebo zasobnik neni prazny !! DOPLNIT
                    rRule[i] = tmp;
                    i++;
                }
                //porovnej rRule s pravidly a na zaklade toho se chovej
                if ((usedRule = findRule(rRule)) == -1) {
                    warning("%d:%d:Probiha psa, hledane pravidlo neexistuje\n", row, character);
                    free(rRule);
                    intStackDispose(&stack);
                    return ERROR_SYNTAX;
                } else {
                    if (intStackPush(&stack, E) == -1) return ERROR_INTERNAL;

                    if ((usedRule > 5) && (leftSide == 1)) { //pokud jsme na leve strane a neprobehlo pravidlo porovnani uprav typ strany
                        expressionTypeLeftside = expressionType;
                    }

                    if ((usedRule > 5) && (leftSide == 0)) { //pokud jsme na prave strane a neprobehlo pravidlo porovnani uprav typ strany
                        expressionTypeRightside = expressionType;
                    }

                    if ((expressionType == BOOLEAN) && ((usedRule == 6) || (usedRule == 7) || (usedRule == 8) || (usedRule == 9) || (usedRule == 11))){
                        warning("%d:%d:Typ BOOLEAN lze pouze porovnavat, zadne jine operace nejsou povoleny\n", row, character);
                        free(rRule);
                        intStackDispose(&stack);
                        return ERROR_SEMANTIC_TYPE;
                    }

                    if ((expressionType == STRING) && ((usedRule == 7) || (usedRule == 8) || (usedRule == 9) || (usedRule == 11))){
                        warning("%d:%d:Typ STRING lze pouze porovnavat a concatenovat(+), zadne jine operace nejsou povoleny\n", row, character);
                        free(rRule);
                        intStackDispose(&stack);
                        return ERROR_SEMANTIC_TYPE;
                    }

                    if (usedRule <= 5) { //probehlo porovnavaci pravidlo, zkotroluju typy, a celkovy typ vyrazu je BOOLEAN
                        if (expressionTypeLeftside != expressionTypeRightside) {
                            warning("%d:%d:Probiha psa, typy na leve a prave strane VYRAZU se neschoduji, leve strana je typu: %d a prava je: %d\n", row, character, expressionTypeLeftside, expressionTypeRightside);
                            free(rRule);
                            intStackDispose(&stack);
                            return ERROR_SEMANTIC_TYPE;
                        }
                        expressionType = BOOLEAN;
                    }
                    if ((token.type <= LEQUAL) && (leftSide == 0)) {
                        warning("%d:%d:Nelze provest vice jak jedno porovnani ve vyrazu.\n", row, character);
                        free(rRule);
                        intStackDispose(&stack);
                        return ERROR_SYNTAX;
                    }


                    if (ruleToInstructin(usedRule) == ERROR_INTERNAL) {
                        return ERROR_INTERNAL;
                    }
                    /*printf("pouzito pravidlo %d, vysledny typ %d.\n", usedRule, type); //POUZITO PRO OSATATNI SYSTEMY*/
                }
                break;
            }
            case EMPTY:
            {
                warning("%d:%d:mismatch in psa operand precedenc table\n", row, character);
                free(rRule);
                intStackDispose(&stack);
                return ERROR_SYNTAX;
            }
        }

        /*assignPreviousToken(&previousToken); unused funnction  */

    } while ((b != DOLLAR) || (intStackFirstTerminal(&stack) != DOLLAR));

    free(rRule);
    intStackDispose(&stack);
    tmp = checkRightToLeftSemantics(type, expressionType); //je potreba nova funkce, checkAritmethicSemantics nestaci protoze neni mozne priradit real do typu int
    if (tmp == ERROR_SEMANTIC_TYPE) { //plus je potreba kontrolovat i typy BOOLEAN, coz checkAritmethicSemantics neumi
        warning("%d:%d: typ vyrazu neodpovida ocekavani leve strany prikazu. Ocekavan typ %d, typ vyrazu %d.\n", row, character, type, expressionType);
        return ERROR_SEMANTIC_TYPE;
    }

    /*printf("Zkoncil jeden expression\n\n");*/

    return OK;
    /*int result;

    if ((result = simpleExpression()) != OK) return result;

    switch (token.type) {
        case EQUAL:
        {
            getTokenMacro(token)
            if ((result = simpleExpression()) != OK) return result;
            break;
        }
        case NOTEQUAL:
        {
            getTokenMacro(token)
            if ((result = simpleExpression()) != OK) return result;
            break;
        }
        case GREATER:
        {
            getTokenMacro(token)
            if ((result = simpleExpression()) != OK) return result;
            break;
        }
        case GEQUAL:
        {
            getTokenMacro(token)
            if ((result = simpleExpression()) != OK) return result;
            break;
        }
        case LESS:
        {
            getTokenMacro(token)
            if ((result = simpleExpression()) != OK) return result;
            break;
        }
        case LEQUAL:
        {
            getTokenMacro(token)
            if ((result = simpleExpression()) != OK) return result;
            break;
        }
    }
    return OK;
     */
}

/*
int simpleExpression(void) {
    int result;

    if ((result = term()) != OK) return result;

    switch (token.type) {
        case PLUS:
        {
            getTokenMacro(token)
            if ((result = simpleExpression()) != OK) return result;
            break;
        }
        case MINUS:
        {
            getTokenMacro(token)
            if ((result = simpleExpression()) != OK) return result;
            break;
        }
    }

    return OK;
}

int term(void) {
    int result;

    if ((result = factor()) != OK) return result;

    switch (token.type) {
        case TIMES:
        {
            getTokenMacro(token)
            if ((result = term()) != OK) return result;
            break;
        }
        case DIVISION:
        {
            getTokenMacro(token)
            if ((result = term()) != OK) return result;
            break;
        }
    }

    return OK;
}

int factor(void) {
    int result = ERROR_SYNTAX;

    switch (token.type) {
        case LBRACKET:
        {
            getTokenMacro(token)
            if ((result = expression()) != OK) return result;
            cmpTokenTypeMacro(token, RBRACKET)
            result = OK;
            getTokenMacro(token)
            break;
        }
        case ID:
        {
            result = OK;
            getTokenMacro(token)
            break;
        }
        case PLUS:
        {
            getTokenMacro(token)
            if ((result = factor()) != OK) return result;
            break;
        }
        case REAL:
        case INTEGER:
        {
            result = OK;
            getTokenMacro(token)
            break;
        }
    }
    if (result != OK) {
        warning("syntax: line:%d: in function:%s: type:%d: expected:%s \n", __LINE__, __func__, token.type, "factor");
    }
    return result;
}
 */

int assignStat(void) {
    int result;
    listitem_t *id;
    int type;

    cmpTokenTypeMacro(token, ID)

    if (localTable != NULL) {
        if ((id = htabLookup(localTable, token.string)) == NULL) {
            id = htabLookup(symbolTable, token.string);
        }
    } else {
        id = htabLookup(symbolTable, token.string);
    }

    if (id != NULL) {
        type = id->type;
    } else {
        warning("%d:%d:semantic:undeclared variable %s\n", row, character, token.string);
        return ERROR_SEMANTIC;
    }

    listitem_t* assignTo = id;

    getTokenMacro(token)
    cmpTokenTypeMacro(token, ASSIGNMENT)

    getTokenMacro(token)

    if (token.type == ID) {
        id = htabLookup(symbolTable, token.string);
        if (id != NULL && id->idType >= DECLARED) { //je to deklarovana nebo definovana fce, POZOR! na zmenu enumu
            getTokenMacro(token)
            if ((result = actuals(id->params, id->key)) == OK) {
                //volani fce
                //musime testovat jestli se nejedena o vestavene funkce, ty jsou specialni pripad, nemaji lokalni tabulky
                if ((strcmp(id->key, "length") == 0)) {
                    listitem_t* p = parseParamsToListitems_t();
                    listitem_t* sourceVariable;
                    sourceVariable = p;
                    if (p->key) {
                        if (localTable != NULL) {
                            if ((sourceVariable = htabLookup(localTable, p->key)) == NULL) {
                                sourceVariable = htabLookup(symbolTable, p->key);
                            }
                        } else {
                            sourceVariable = htabLookup(symbolTable, p->key);
                        }
                    }
                    generateInstruction(I_J, id, sourceVariable, NULL);
                    generateInstruction(I_ASSIGN, NULL, NULL, assignTo);
                    paramItemFree(&listOfParams);
                    return OK;
                }
                if ((strcmp(id->key, "copy") == 0)) {
                    listitem_t* p = parseParamsToListitems_t();
                    listitem_t* sourceVariable;
                    sourceVariable = p;
                    if (p->key) {
                        if (localTable != NULL) {
                            if ((sourceVariable = htabLookup(localTable, p->key)) == NULL) {
                                sourceVariable = htabLookup(symbolTable, p->key);
                            }
                        } else {
                            sourceVariable = htabLookup(symbolTable, p->key);
                        }
                    }
                    generateInstruction(I_PUSH, p->next->next, NULL, NULL);
                    generateInstruction(I_J, id, sourceVariable, p->next);
                    generateInstruction(I_ASSIGN, NULL, NULL, assignTo);
                    paramItemFree(&listOfParams);
                    return OK;
                }
                if ((strcmp(id->key, "find") == 0)) {
                    listitem_t* p = parseParamsToListitems_t();
                    listitem_t* sourceVariable;
                    sourceVariable = p;
                    if (p->key) {
                        if (localTable != NULL) {
                            if ((sourceVariable = htabLookup(localTable, p->key)) == NULL) {
                                sourceVariable = htabLookup(symbolTable, p->key);
                            }
                        } else {
                            sourceVariable = htabLookup(symbolTable, p->key);
                        }
                    }
                    listitem_t* sourceVariable2;
                    sourceVariable2 = p->next;
                    if (p->next->key) {
                        if (localTable != NULL) {
                            if ((sourceVariable2 = htabLookup(localTable, p->next->key)) == NULL) {
                                sourceVariable2 = htabLookup(symbolTable, p->next->key);
                            }
                        } else {
                            sourceVariable2 = htabLookup(symbolTable, p->next->key);
                        }
                    }
                    generateInstruction(I_J, id, sourceVariable, sourceVariable2);
                    generateInstruction(I_ASSIGN, NULL, NULL, assignTo);
                    paramItemFree(&listOfParams);
                    return OK;
                }
                if ((strcmp(id->key, "sort") == 0)) {
                    listitem_t* p = parseParamsToListitems_t();
                    listitem_t* sourceVariable;
                    sourceVariable = p;
                    if (p->key) {
                        if (localTable != NULL) {
                            if ((sourceVariable = htabLookup(localTable, p->key)) == NULL) {
                                sourceVariable = htabLookup(symbolTable, p->key);
                            }
                        } else {
                            sourceVariable = htabLookup(symbolTable, p->key);
                        }
                    }
                    generateInstruction(I_J, id, sourceVariable, NULL);
                    generateInstruction(I_ASSIGN, NULL, NULL, assignTo);
                    paramItemFree(&listOfParams);
                    return OK;
                }
                if (generateInstruction(I_FCE_START, id, NULL, NULL) == NULL) { //skok na zacatek instrukci funkce
                    return ERROR_INTERNAL;
                }
                //
                //intrukce pro:
                if ((result = generateInstructionsToCopyParams(id)) != OK) {//zkopirovani parametru
                    return result;
                }
                if (generateInstruction(I_J, id, NULL, id->instructions) == NULL) { //skok na zacatek instrukci funkce
                    return ERROR_INTERNAL;
                }

                generateInstruction(I_ASSIGN, NULL, NULL, assignTo);

                paramItemFree(&listOfParams);
                return OK;
            } else {
                return result;
            }
        }

        //        if (funkce != NULL && funkce->idType >= DECLARED) { //je to deklarovana nebo definovana fce, POZOR! na zmenu enumu
        //            if ((result = functionCall()) != OK) return result;
        //
        //        }
    }
    if ((result = expression(type)) != OK) return result;
    if (generateInstruction(I_ASSIGN, NULL, NULL, assignTo) == NULL) {
        return ERROR_INTERNAL;
    } //prirazuju vrchol zasobniku
    //getTokenMacro(token)
    /*cmpTokenTypeMacro(token, SEMICOLON) toto porovnava az statlistu, tady nevim jestli nejsem posledni mezi BEGIN a END*/

    /*getTokenMacro(token) ten strednik porad potrebujem, na porovnani jestli je v statListu jeden prikaz nebo vice*/
    return OK;
}

int actuals(paramItem_t* paramsDefined, const char* key) {
    int result;
    cmpTokenTypeMacro(token, LBRACKET)
    listOfParams = NULL;
    getTokenMacro(token)
    result = paramList();
    if (result != OK) {
        return result;
    }
    paramItem_t* tempHeadParam = listOfParams;
    if (result != OK) {
        paramItemFree(&listOfParams);
        return result;
    }

    while (paramsDefined && tempHeadParam) { //cyklus porovnava shodu parametru volane a definove fce
        if (paramsDefined->type == tempHeadParam->type) {
            paramsDefined = paramsDefined->next;
            tempHeadParam = tempHeadParam->next;
        } else {
            warning("%d:%d:Semantic:parameters of call & definition of %s do not match!\n", row, character, key);
            return ERROR_SEMANTIC_TYPE;
        }
    }
    if (!(tempHeadParam) && !(paramsDefined)) { //pokud je definovanych i deklarovanych stejny pocet
        result = OK;
    } else {
        warning("%d:%d:Semantic:parameters of call & definition of %s do not match!\n", row, character, key);
        result = ERROR_SEMANTIC_TYPE;
    }
    cmpTokenTypeMacro(token, RBRACKET)
    /*
        printf("Kontrolni tisk zesnamu parametru:\n");
        while(tempHeadParam) {
            printf("Parametr %s, typu: %d, s hodnotou %f, nebo retezcem: %s.\n", tempHeadParam->string, tempHeadParam->type, tempHeadParam->value, tempHeadParam->string);
            tempHeadParam = tempHeadParam->next;
        }
     */
    getTokenMacro(token)

    return result;
}

int paramList(void) { //mozne vylepseni, zavest kontolu alokaci pro parametry
    int result;
    listitem_t *id;
    if (token.type == RBRACKET) {
        return OK;
    }
    if (token.type == ID) {

        if (localTable != NULL) {
            if ((id = htabLookup(localTable, token.string)) == NULL) {
                id = htabLookup(symbolTable, token.string);
            }
        } else {
            id = htabLookup(symbolTable, token.string);
        }

        if (id == NULL) {
            warning("%d:%d:Semantic: undeclared variable: %s\n", row, character, token.string);
            return ERROR_SEMANTIC;
        }
        paramItemAdd(&listOfParams, '\0', id->type);
        paramItemUpdate(&listOfParams, NULL, 0, token.string);
        getTokenMacro(token)
        if (token.type == COMMA) {
            getTokenMacro(token)
            result = paramList();
            return result;
        }
        return OK;
    }
    if (token.type == INTEGER) {
        paramItemAdd(&listOfParams, '\0', TINTEGER);
        paramItemUpdate(&listOfParams, NULL, atoi(token.string), NULL);
        getTokenMacro(token)
        if (token.type == COMMA) {
            getTokenMacro(token)
            result = paramList();
            return result;
        }
        return OK;
    }
    if (token.type == BOOLEAN) {
        paramItemAdd(&listOfParams, '\0', TBOOLEAN);
        paramItemUpdate(&listOfParams, NULL, atoi(token.string), NULL);
        getTokenMacro(token)
        if (token.type == COMMA) {
            getTokenMacro(token)
            result = paramList();
            return result;
        }
        return OK;
    }

    if (token.type == REAL) {
        paramItemAdd(&listOfParams, '\0', TREAL);
        paramItemUpdate(&listOfParams, NULL, atof(token.string), NULL);
        getTokenMacro(token)
        if (token.type == COMMA) {
            getTokenMacro(token)
            result = paramList();
            return result;
        }
        return OK;
    }

    if (token.type == STRING) {
        paramItemAdd(&listOfParams, '\0', TSTRING);
        paramItemUpdate(&listOfParams, token.string, 0, NULL);
        getTokenMacro(token)
        if (token.type == COMMA) {
            getTokenMacro(token)
            result = paramList();
            return result;
        }
        return OK;
    }

    return OK;
}

int ifStat(void) {
    //chce if pak expression, pak then, pak budto coumpndStat nebo statList, zalezi jestli je BEGIN, pak je mozne jeste else 
    //pokud je else, to same jako po then
    int result;
    int type = BOOLEAN;
    cmpTokenTypeMacro(token, IF)

    getTokenMacro(token)
    if ((result = expression(type)) != OK) return result;

    tListItem* jToElse = generateInstruction(I_JZERO, NULL, NULL, NULL); //porovnava se hodnota ze zasobniku, nakonci se nastavuje kam skakat, na ktere navesti.
    if (jToElse == NULL) {
        return ERROR_INTERNAL;
    }
    cmpTokenTypeMacro(token, THEN)
    getTokenMacro(token)

    if ((result = coumpndStat()) != OK) return result;

    tListItem* jAroundElse = generateInstruction(I_J, NULL, NULL, NULL); //skace se vzdy, obskakuju else, adresa se doplni na konci ifu.
    if (jAroundElse == NULL) {
        return ERROR_INTERNAL;
    }
    cmpTokenTypeMacro(token, ELSE)

    tListItem* elseBegin = generateInstruction(I_LABEL, NULL, NULL, NULL);
    if (elseBegin == NULL) {
        return ERROR_INTERNAL;
    }
    getTokenMacro(token)
    if ((result = coumpndStat()) != OK) return result;

    tListItem* elseEnd = generateInstruction(I_LABEL, NULL, NULL, NULL);
    if (elseEnd == NULL) {
        return ERROR_INTERNAL;
    }
    updateInstructionResult(jToElse, elseBegin); //aktualizace instrukci kam se skace
    updateInstructionResult(jAroundElse, elseEnd);

    return OK;
}

int whileStat(void) {
    int result;
    int type = BOOLEAN;
    cmpTokenTypeMacro(token, WHILE)

    tListItem* whileBegin = generateInstruction(I_LABEL, NULL, NULL, NULL);
    if (whileBegin == NULL) {
        return ERROR_INTERNAL;
    }
    getTokenMacro(token)
    if ((result = expression(type)) != OK) return result;

    cmpTokenTypeMacro(token, DO)

    tListItem* jToWhileEnd = generateInstruction(I_JZERO, NULL, NULL, NULL); //porovnava se hodnota ze zasobniku, nakonci se nastavuje kam skakat, na ktere navesti.
    if (jToWhileEnd == NULL) {
        return ERROR_INTERNAL;
    }
    getTokenMacro(token)
    if ((result = coumpndStat()) != OK) return result;

    if (generateInstruction(I_J, NULL, NULL, whileBegin) == NULL) {
        return ERROR_INTERNAL;
    } //skace se vzdy, pred vyhodnoceni podminky while
    tListItem* whileEnd = generateInstruction(I_LABEL, NULL, NULL, NULL);
    if (whileEnd == NULL) {
        return ERROR_INTERNAL;
    }
    updateInstructionResult(jToWhileEnd, whileEnd); //aktualizace instrukci kam se skace

    return OK;
}

int checkAritmethicSemantics(int a, int b) { //zavorky jsou nutne, z hlediska semantky jakoby nemaji typ, tzn. muzou se vyskytnout kdekoli
    if (((a == INTEGER) || (a == TINTEGER) || (a == RBRACKET) || (a == LBRACKET)) && ((b == INTEGER) || (b == TINTEGER) )) {
        return INTEGER;
    }

    if (((a == INTEGER) || (a == TINTEGER)) && ((b == INTEGER) || (b == TINTEGER) || (b == RBRACKET) || (b == LBRACKET))) {
        return INTEGER;
    }
    if (((a == INTEGER) || (a == TINTEGER) || (a == RBRACKET) || (a == LBRACKET)) && ((b == REAL) || (b == TREAL))) {
        return REAL;
    }

    if (((a == INTEGER) || (a == TINTEGER)) && ((b == REAL) || (b == TREAL) || (b == RBRACKET) || (b == LBRACKET))) {
        return REAL;
    }

    if (((a == REAL) || (a == TREAL) || (a == RBRACKET) || (a == LBRACKET)) && ((b == INTEGER) || (b == TINTEGER) )) {
        return REAL;
    }

    if (((a == REAL) || (a == TREAL)) && ((b == INTEGER) || (b == TINTEGER) || (b == RBRACKET) || (b == LBRACKET))) {
        return REAL;
    }
    if (((a == REAL) || (a == TREAL) || (a == RBRACKET) || (a == LBRACKET)) && ((b == REAL) || (b == TREAL))) {
        return REAL;
    }
    if (((a == REAL) || (a == TREAL)) && ((b == REAL) || (b == TREAL) || (b == RBRACKET) || (b == LBRACKET))) {
        return REAL;
    }
    if (((a == STRING) || (a == TSTRING) || (a == RBRACKET) || (a == LBRACKET)) && ((b == STRING) || (b == TSTRING))) {
        return STRING;
    }

    if (((a == STRING) || (a == TSTRING)) && ((b == STRING) || (b == TSTRING) || (b == RBRACKET) || (b == LBRACKET))) {
        return STRING;
    }

    if (((a == BOOLEAN) || (a == TBOOLEAN) || (a == RBRACKET) || (a == LBRACKET)) && ((b == BOOLEAN) || (b == TBOOLEAN))) {
        return BOOLEAN;
    }

    if (((a == BOOLEAN) || (a == TBOOLEAN)) && ((b == BOOLEAN) || (b == TBOOLEAN) || (b == RBRACKET) || (b == LBRACKET))) {
        return BOOLEAN;
    }

    if (((a == RBRACKET) || (a == LBRACKET)) && ((b == RBRACKET) || (b == LBRACKET))) {
        return LBRACKET;
    }
    return ERROR_SEMANTIC_TYPE;
}

int checkRightToLeftSemantics(int a, int b) {
    if (((a == INTEGER) || (a == TINTEGER)) && ((b == INTEGER) || (b == TINTEGER))) {
        return INTEGER;
    }

    if (((a == REAL) || (a == TREAL)) && ((b == REAL) || (b == TREAL))) {
        return REAL;
    }

    if (((a == STRING) || (a == TSTRING)) && ((b == STRING) || (b == TSTRING))) {
        return STRING;
    }

    if (((a == BOOLEAN) || (a == TBOOLEAN)) && ((b == BOOLEAN) || (b == TBOOLEAN))) {
        return BOOLEAN;
    }

    return ERROR_SEMANTIC_TYPE;
}

listitem_t* tokenToListitem_t(void) {

    listitem_t* p;


    if (token.type == ID) {
        if (localTable != NULL) {
            if ((p = htabLookup(localTable, token.string)) == NULL) {
                p = htabLookup(symbolTable, token.string);
            }
        } else {
            p = htabLookup(symbolTable, token.string);
        }
        if (p == NULL) {
            return NULL;
        }
    } else {
        if ((p = malloc(sizeof (struct htabListitem))) == NULL) {
            warning("%d:%d:Interni chyba pri alokaci pameti pro konkretni vstupni hodnoty/n", row, character);
            return NULL;
        }
        p->key = NULL;
        p->string = NULL;
        p->next = NULL;
        p->idType = INIT;
        /*p->type = token.type;*/
        if (token.type == INTEGER) p->type = TINTEGER;
        if (token.type == REAL) p->type = TREAL;
        if (token.type == BOOLEAN) p->type = TBOOLEAN;
        if (token.type == STRING) p->type = TSTRING;


        if (token.type == STRING) {
            char *string;
            if ((string = malloc(sizeof (char) *(strlen(token.string) + 1))) == NULL) {
                warning("internal: memory allocation failed for %s name\n", token.string);
                return NULL;
            }
            strcpy(string, token.string);
            p->string = string;
        } else if (token.type == REAL) {
            p->value = atof(token.string);
        } else if (token.type == INTEGER) {
            p->value = atoi(token.string);
        } else if (token.type == BOOLEAN) {
            p->value = atoi(token.string);
        }
        if (addNotFreeItem(p) != OK){
            return NULL;
        }
    }

    /*printf("klic: %s, hodnota: %f, string: %s\n", p->key, p->value, p->string); //kontrolni vystup*/

    return p;
}

int ruleToInstructin(int usedRule) {

    switch (usedRule) {
        case 0:
            if (generateInstruction(I_EQUAL, NULL, NULL, NULL) == NULL) {
                return ERROR_INTERNAL;
            }
            break;
        case 1:
            if (generateInstruction(I_NEQUAL, NULL, NULL, NULL) == NULL) {
                return ERROR_INTERNAL;
            }
            break;
        case 2:
            if (generateInstruction(I_LESS, NULL, NULL, NULL) == NULL) {
                return ERROR_INTERNAL;
            }
            break;
        case 3:
            if (generateInstruction(I_LEQUAL, NULL, NULL, NULL) == NULL) {
                return ERROR_INTERNAL;
            }
            break;
        case 4:
            if (generateInstruction(I_GREATER, NULL, NULL, NULL) == NULL) {
                return ERROR_INTERNAL;
            }
            break;
        case 5:
            if (generateInstruction(I_GEQUAL, NULL, NULL, NULL) == NULL) {
                return ERROR_INTERNAL;
            }
            break;
        case 6:
            if (generateInstruction(I_ADD, NULL, NULL, NULL) == NULL) {
                return ERROR_INTERNAL;
            }
            break;
        case 7:
            if (generateInstruction(I_SUB, NULL, NULL, NULL) == NULL) {
                return ERROR_INTERNAL;
            }
            break;
        case 8:
            if (generateInstruction(I_MUL, NULL, NULL, NULL) == NULL) {
                return ERROR_INTERNAL;
            }
            break;
        case 9:
            if (generateInstruction(I_DIV, NULL, NULL, NULL) == NULL) {
                return ERROR_INTERNAL;
            }
            break;
    }
    return OK;
}

int updateFuncInstruction(char* id, tListItem * functionBegin) {
    listitem_t* p;
    if ((p = htabLookup(symbolTable, id)) == NULL) {
        warning("%d:%d:Pri updateovani instrukci, tzn. kdyz se dosazovali do funkce, funkce nebyla nalezena\n");
        return ERROR_SEMANTIC;
    }
    p->instructions->instr.operation = I_LABEL;
    p->instructions->nextItem = functionBegin;
    return OK;
}

int generateInstructionsToCopyParams(listitem_t * id) {
    listitem_t* actual = parseParamsToListitems_t();
    paramItem_t* defined = id->params;
    char* name;
    listitem_t* localVariable;
    listitem_t* sourceVariable;

    while (actual) {
        name = defined->id;
        if ((localVariable = htabLookup(id->local, name)) == NULL) {
            warning("%d:%d:Promenna %s nebyla nalezena pri generovani instrukce, ktera by ji plnila hodnotou parametru pri volani funkce.\n", row, character, id->key);
            return ERROR_SEMANTIC;
        }

        if (actual->key != NULL) {

            if (localTable != NULL) {
                if ((sourceVariable = htabLookup(localTable, actual->key)) == NULL) {
                    sourceVariable = htabLookup(symbolTable, actual->key);
                }
            } else {
                sourceVariable = htabLookup(symbolTable, actual->key);
            }

            if (generateInstruction(I_ASSIGN, sourceVariable, NULL, localVariable) == NULL) {
                return ERROR_INTERNAL;
            }
            defined = defined->next;
            actual = actual->next;
            continue;
        }

        switch (actual->type) {
            case TINTEGER:
                if (generateInstruction(I_ASSIGN, actual, NULL, localVariable) == NULL) {
                    return ERROR_INTERNAL;
                }
                break;

            case TREAL:
                if (generateInstruction(I_ASSIGN, actual, NULL, localVariable) == NULL) {
                    return ERROR_INTERNAL;
                }
                break;

            case TBOOLEAN:
                if (generateInstruction(I_ASSIGN, actual, NULL, localVariable) == NULL) {
                    return ERROR_INTERNAL;
                }
                break;

            case TSTRING:
                if (generateInstruction(I_ASSIGN, actual, NULL, localVariable) == NULL) {
                    return ERROR_INTERNAL;
                }
                break;
        }

        defined = defined->next;
        actual = actual->next;
    }


    return OK;
}

listitem_t * parseParamsToListitems_t(void) {
    paramItem_t* p = listOfParams;
    listitem_t* head;
    head = NULL;
    listitem_t* actual;

    while (p) { //mohla by byt samostatna fce, POTREBUJU zkopirovat parametry do polezek listitem_t, potrebuju taky poresit jejich uvolnovani
        listitem_t* new;
        new = malloc(sizeof (listitem_t)); 
        if (new == NULL){
            warning("%d:%d:internal: memory allocation failed when parsing params to listitems_t\n", row, character);
            return NULL;
        }
        new->type = p->type;
        new->next = NULL;
        new->string = '\0';
        new->value = 0;
        new->key = '\0';
        new->idType = INIT;
        if (p->id != NULL) {
            new->key = malloc(sizeof (char) * (strlen(p->id) + 1));
            if (new->key == NULL){
                warning("%d:%d:internal: memory allocation failed when parsing params to listitems_t\n", row, character);
                return NULL;
            }
            strcpy((char*) new->key, p->id);
        }
        if (p->string != NULL) {
            new->string = malloc(sizeof (char) * (strlen(p->string) + 1)); 
            if (new->string == NULL){
                warning("%d:%d:internal: memory allocation failed when parsing params to listitems_t\n", row, character);
                return NULL;
            }
            strcpy(new->string, p->string);
            new->value = 0; //init
        } else {
            new->value = p->value;
            new->string = '\0'; //init
        }
        if (!head) {
            actual = new;
            head = new;
        } else {
            head->next = new;
            head = head->next;
        }
        p = p->next;
        if (addNotFreeItem(new) != OK){
            return NULL;
        }
    }

    return actual;
}
