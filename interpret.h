/*********************************************************************/
/*																	 */
/*		ifj14										    		     */
/*Název projektu: Implementace interpretu imperativního jazyka IFJ14.*/
/*********************************************************************/
/*																	 */
/*		Matěj Aleš		xmatej48									 */
/*		Uherek David	xuhere03									 */
/*		Velecký Lukáš	xvelec05      								 */
/*		Zemek Martin	xzemek04									 */
/*																	 */
/*********************************************************************/

#ifndef INTERPRET_H
#define	INTERPRET_H


#include "generator.h"


typedef struct stackDatV{
        double value;
        struct stackDatV *next;
}stack_v;

typedef struct stackDatV *stackV;


typedef struct stackDatS{
        char* string;
        struct stackDatS *next;
}stack_s;

typedef struct stackDatS *stackS;


typedef struct stackInt{
        tListItem* ins;
        struct stackInt *next;
}stack_inst;

typedef struct stackInt *stackInsInt;


void stackInitV(stackV *s);
double stackTopV(stackV *s);
void stackPopV(stackV *s);
int stackPushV(stackV *s, double v);
void stackDisposeV(stackV *s);
void stackInitS(stackS *s);
char* stackTopS(stackS *s);
void stackPopS(stackS *s);
int stackPushS(stackS *s, char* str);
void stackDisposeS(stackS *s);
void stackInitInt(stackInsInt *s);
tListItem* stackTopInt(stackInsInt *s);
void stackPopInt(stackInsInt *s);
int stackPushInt(stackInsInt *s, tListItem* ins);
void stackDisposeInt(stackInsInt *s);
char* concatenate(char *str1, char *str2);
int interpret(tInstr **table, tListOfInstr *originalInstructionList);
int isInteger(char* cislo);
int isReal(char* cislo);
int readInf(char** string);

#endif	/* INTERPRET_H */
